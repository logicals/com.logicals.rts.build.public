project("RTSBuild" LANGUAGES C CXX)
cmake_minimum_required(VERSION 3.6 FATAL_ERROR)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/../cmake/Modules")

include(CommonTargets)
