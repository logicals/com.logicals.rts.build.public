# Name of the target platform
set(CMAKE_SYSTEM_NAME BareMetalSTWESX)
set(CMAKE_SYSTEM_VERSION 1)

if(IS_ABSOLUTE ${TRICORE_LDSCRIPT_DIR}) #condition is false, if variable is not defined
  set(LD_SCRIPT_DIR ${TRICORE_LDSCRIPT_DIR})
else()
  set(LD_SCRIPT_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../../ldscripts/${TRICORE_LDSCRIPT_DIR})
endif()

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  set(TRICORE_BIN_DIR "${RTS_ENV_BIN_HOME}/compiler/TricoreGCC/v4.9.4.0/bin")
  set(TRICORE_COMPILER ${TRICORE_BIN_DIR}/tricore-gcc.exe)
  set(TRICORE_SIZE ${TRICORE_BIN_DIR}/tricore-size.exe)
  set(TRICORE_OBJCOPY ${TRICORE_BIN_DIR}/tricore-objcopy.exe)
  set(TRICORE_SIGNATURE_GEN ${RTS_ENV_BIN_HOME}/tools/STWTools/openSYDEFlashSignatureGenerator.exe)
  set(TRICORE_NM ${TRICORE_BIN_DIR}/tricore-nm.exe)
  set(TRICORE_OBJDUMP ${TRICORE_BIN_DIR}/tricore-objdump.exe)
else()
  MESSAGE(FATAL_ERROR Unsupported build platform.)
endif()

set(CMAKE_C_COMPILER ${TRICORE_COMPILER})
set(CMAKE_ASM_COMPILER ${TRICORE_COMPILER})

set(TRICORE_FLAGS "-mtc162 -g2 -Os -std=gnu99 -fno-common -ffunction-sections -fdata-sections -Wall -Wdouble-promotion -Wstrict-prototypes")
#set(CMAKE_ASM_FLAGS "${TRICORE_FLAGS} -x assembler-with-cpp" CACHE STRING "")
set(CMAKE_C_FLAGS "${TRICORE_FLAGS}" CACHE STRING "")
set(CMAKE_C_FLAGS_RELEASE "" CACHE STRING "")
set(CMAKE_C_FLAGS_DEBUG "" CACHE STRING "")
set(CMAKE_EXE_LINKER_FLAGS "-mtc162 -nocrt0 -Wl,--mem-holes -Wl,-gc-sections -L ${LD_SCRIPT_DIR} -T ${TRICORE_LDSCRIPT} -Wl,-Map=ESX4Cx.gcc.map" CACHE STRING "")

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_C_LINK_FLAGS> <LINK_FLAGS>" CACHE STRING "") 

### special targets

### add_size_info

function(add_size_info targetName exe)
  add_custom_target(${targetName} ALL
    COMMAND ${TRICORE_SIZE} $<TARGET_FILE:${exe}>
  )
endfunction()

### add_binary
function(add_binary binary exe)
  set(binaryFileTmp ${CMAKE_CURRENT_BINARY_DIR}/${binary}.tmp.hex)
  set(binaryFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.hex)
  add_custom_command(
    OUTPUT ${binaryFile}
    COMMAND ${TRICORE_OBJCOPY} -O ihex $<TARGET_FILE:${exe}> ${binaryFileTmp}
    COMMAND ${TRICORE_SIGNATURE_GEN} -i ${binaryFileTmp} -o ${binaryFile}
    DEPENDS ${exe}
  )

  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${binaryFile}
  )

  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${binaryFile}
  )
endfunction()

### add_md_binary

function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${TRICORE_OBJCOPY} -D OBJDUMP=${TRICORE_OBJDUMP} -D NM=${TRICORE_NM}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=${TRICORE_MD_DATA_SEGMENT}
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()
