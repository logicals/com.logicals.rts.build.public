set(WIN32 0)
set(UNIX 1)
set(CMAKE_SYSTEM_NAME ESP8266)
set(CMAKE_SYSTEM_VERSION 1)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/Modules")

set (ESP8266_FLASH_SIZE "512K" CACHE STRING "Size of flash")

if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
    set(HOST_EXECUTABLE_PREFIX "")
    set(TOOLCHAIN_DIR "/usr/local/toolchains/xtensa-lx106-elf")
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
    set(HOST_EXECUTABLE_PREFIX ".exe")
    set(TOOLCHAIN_DIR "${RTS_ENV_BIN_HOME}/compiler/xtensa-lx106-elf")
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()

set(ESP8266_XTENSA_C_COMPILER ${TOOLCHAIN_DIR}/bin/xtensa-lx106-elf-gcc${HOST_EXECUTABLE_PREFIX})
set(ESP8266_XTENSA_CXX_COMPILER ${TOOLCHAIN_DIR}/bin/xtensa-lx106-elf-g++${HOST_EXECUTABLE_PREFIX})
set(ESP8266_XTENSA_OBJCOPY ${TOOLCHAIN_DIR}/bin/xtensa-lx106-elf-objcopy${HOST_EXECUTABLE_PREFIX})
set(ESP8266_XTENSA_OBJDUMP ${TOOLCHAIN_DIR}/bin/xtensa-lx106-elf-objdump${HOST_EXECUTABLE_PREFIX})
set(ESP8266_XTENSA_NM ${TOOLCHAIN_DIR}/bin/xtensa-lx106-elf-nm${HOST_EXECUTABLE_PREFIX})

set(CMAKE_C_COMPILER ${ESP8266_XTENSA_C_COMPILER})
set(CMAKE_CXX_COMPILER ${ESP8266_XTENSA_CXX_COMPILER})

set(CMAKE_C_FLAGS "-Os -g -std=gnu99 -Wpointer-arith -Wno-implicit-function-declaration -Wundef -pipe -D__ets__ -DICACHE_FLASH -fno-inline-functions -ffunction-sections -nostdlib -mlongcalls -mtext-section-literals -falign-functions=4 -fdata-sections" CACHE STRING "")
set(CMAKE_CXX_FLAGS "-Os -g -D__ets__ -DICACHE_FLASH -mlongcalls -mtext-section-literals -fno-exceptions -fno-rtti -falign-functions=4 -std=c++11 -MMD -ffunction-sections -fdata-sections" CACHE STRING "")
set(CMAKE_EXE_LINKER_FLAGS "-nostdlib -Wl,--no-check-sections -Wl,-static -Wl,--gc-sections" CACHE STRING "")

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group")

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${ESP8266_XTENSA_OBJCOPY} -D NM=${ESP8266_XTENSA_NM} -D OBJDUMP=${ESP8266_XTENSA_OBJDUMP}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=.rodata
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()
