# Name of the target platform
set(WIN32 0)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(DIR_COMPILER /usr/local/toolchains/gcc-linaro-7.2.1-2017.11-x86_64_arm-linux-gnueabihf)
  set(CMAKE_C_COMPILER ${DIR_COMPILER}/bin/arm-linux-gnueabihf-gcc)
  set(LINUX_OBJCOPY ${DIR_COMPILER}/bin/arm-linux-gnueabihf-objcopy)
  set(LINUX_NM ${DIR_COMPILER}/bin/arm-linux-gnueabihf-nm)
  set(LINUX_OBJDUMP ${DIR_COMPILER}/bin/arm-linux-gnueabihf-objdump)
else()
  string(REPLACE "\\" "/" RTS3_COMPILER_HOME $ENV{RTS3_COMPILER_HOME})
  set(DIR_COMPILER ${RTS3_COMPILER_HOME}/gcc-linaro-7.2.1-2017.11-i686-mingw32_arm-linux-gnueabihf)
  set(CMAKE_C_COMPILER ${DIR_COMPILER}/bin/arm-linux-gnueabihf-gcc.exe)
  set(LINUX_OBJCOPY ${DIR_COMPILER}/bin/arm-linux-gnueabihf-objcopy.exe)
  set(LINUX_NM ${DIR_COMPILER}/bin/arm-linux-gnueabihf-nm.exe)
  set(LINUX_OBJDUMP ${DIR_COMPILER}/bin/arm-linux-gnueabihf-objdump.exe)
endif()

set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration -march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_RELEASE "-O2" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_DEBUG "-O0 -g" CACHE STRING "" FORCE)

set(CMAKE_EXE_LINKER_FLAGS "-Wl,--no-undefined" CACHE STRING "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "" CACHE STRING "" FORCE)

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_C_LINK_FLAGS> <LINK_FLAGS>")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS>")

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${LINUX_OBJCOPY} -D NM=${LINUX_NM} -D OBJDUMP=${LINUX_OBJDUMP}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=.rodata
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()
