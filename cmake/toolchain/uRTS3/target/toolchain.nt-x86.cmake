# Name of the target platform
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_SYSTEM_NAME WindowsCross)
set(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  set(CMAKE_C_COMPILER ${RTS_ENV_BIN_HOME}/compiler/mingw32/bin/gcc.exe)
  set(WIN32_OBJCOPY ${RTS_ENV_BIN_HOME}/compiler/mingw32/bin/objcopy.exe)
  set(WIN32_OBJDUMP ${RTS_ENV_BIN_HOME}/compiler/mingw32/bin/objdump.exe)
  set(WIN32_NM ${RTS_ENV_BIN_HOME}/compiler/mingw32/bin/nm.exe)
  set(CMAKE_RC_COMPILER ${RTS_ENV_BIN_HOME}/compiler/mingw32/bin/windres.exe CACHE FILEPATH "" FORCE)
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CMAKE_C_COMPILER i686-w64-mingw32-gcc)
  set(WIN32_OBJCOPY i686-w64-mingw32-objcopy)
  set(WIN32_OBJDUMP i686-w64-mingw32-objdump)
  set(WIN32_NM i686-w64-mingw32-nm)
  set(CMAKE_RC_COMPILER_ENV_VAR "RC")
  set($ENV{RC} "")
  set(CMAKE_RC_COMPILER "")
else()
  message(FATAL_ERROR Unsupported build platform.)
endif()

set(CMAKE_C_FLAGS "-std=c99 -g -pedantic -Wall -Wpointer-arith -pipe -Wno-unused-parameter -Wno-unused-variable" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_RELEASE "-O0" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_DEBUG "-O0" CACHE STRING "" FORCE)

set(CMAKE_EXE_LINKER_FLAGS "-static -lwinmm -lpthread -lws2_32 -lwsock32" CACHE STRING "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "" CACHE STRING "" FORCE)

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_C_LINK_FLAGS> <LINK_FLAGS>")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS>")

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${WIN32_OBJCOPY} -D NM=${WIN32_NM} -D OBJDUMP=${WIN32_OBJDUMP}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=.rdata
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()
