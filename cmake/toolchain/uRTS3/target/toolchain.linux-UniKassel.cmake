# Name of the target platform
set(WIN32 0)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)

set(UC_LINUX_DIST_DIR /opt/UK/uClinux-dist-2014)
set(UC_COMPILER_DIR /opt/UK/m68k-uclinux_4.5/bin)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CMAKE_C_COMPILER ${UC_COMPILER_DIR}/m68k-uclinux-gcc)
  set(LINUX_OBJCOPY ${UC_COMPILER_DIR}/m68k-uclinux-objcopy)
  set(LINUX_NM ${UC_COMPILER_DIR}/m68k-uclinux-nm)
  set(LINUX_OBJDUMP ${UC_COMPILER_DIR}/m68k-uclinux-objdump)
else()
  message(FATAL_ERROR "invalid host platform")
endif()

set(CMAKE_C_FLAGS "-std=gnu99 -nostdinc -idirafter ${UC_LINUX_DIST_DIR}/staging/include -isystem ${UC_LINUX_DIST_DIR}/staging/uClibc/include -isystem ${UC_COMPILER_DIR}/../lib/gcc/m68k-uclinux/4.5.1/include-fixed -isystem ${UC_COMPILER_DIR}/../lib/gcc/m68k-uclinux/4.5.1/include -m5307 -DCONFIG_COLDFIRE -Os -g -fomit-frame-pointer -pipe -fno-common -fno-builtin -Wall -DEMBED -msep-data -Dlinux -D__linux__ -Dunix -D__uClinux__" CACHE STRING "" FORCE)

set(URTS_BUILD_METADATA false CACHE BOOL "build metadata instead of uRTS")

if(URTS_BUILD_METADATA)
  set(UC_LINKER_FLAGS_OUTPUT_FMT "-Wl,--oformat -Wl,elf32-m68k")
else()
  set(UC_LINKER_FLAGS_OUTPUT_FMT "-Wl,-elf2flt -Wl,-move-rodata -msep-data")
endif()

set(CMAKE_EXE_LINKER_FLAGS "-nostdlib -m5307 -DCONFIG_COLDFIRE ${UC_LINKER_FLAGS_OUTPUT_FMT} -L ${UC_LINUX_DIST_DIR}/uClibc/lib -Wl,-rpath-link,${UC_LINUX_DIST_DIR}/uClibc/lib -L ${UC_LINUX_DIST_DIR}/staging/lib -Wl,-rpath-link,${UC_LINUX_DIST_DIR}/staging/lib" CACHE STRING "" FORCE)

set(UC_SYS_OBJ "${UC_LINUX_DIST_DIR}/uClibc/lib/crt1.o ${UC_LINUX_DIST_DIR}/uClibc/lib/crti.o ${UC_LINUX_DIST_DIR}/uClibc/lib/crtn.o")
set(UC_SYS_LIBS "-lc -lgcc")

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_C_LINK_FLAGS> <LINK_FLAGS>")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS>")

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${LINUX_OBJCOPY} -D NM=${LINUX_NM} -D OBJDUMP=${LINUX_OBJDUMP}
      -D EXECUTABLE=$<TARGET_FILE:${exe}>.gdb -D MD_FILE=${mdFile}
      -D SEGMENT=.data
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()

