# Name of the target platform
set(WIN32 0)
set(CMAKE_SYSTEM_NAME BareMetalCortexM4)
set(CMAKE_SYSTEM_VERSION 1)

set(LD_SCRIPT_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../../ldscripts)

set(ARM_DIR c:/rts3env/compiler/MDK523/ARM)
set(ARM_COMPILER_DIR "${ARM_DIR}/ARMCC/bin")

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  set(CORTEXM4_COMPILER ${ARM_COMPILER_DIR}/armcc.exe)
  set(CORTEXM4_ASM ${ARM_COMPILER_DIR}/armasm.exe)
  set(CORTEXM4_HEXGEN ${ARM_COMPILER_DIR}/fromelf.exe)
else()
  MESSAGE(FATAL_ERROR Unsupported build platform.)
endif()

set(CMAKE_C_COMPILER ${CORTEXM4_COMPILER})
set(CMAKE_CXX_COMPILER ${CORTEXM4_COMPILER})
set(CMAKE_ASM_COMPILER ${CORTEXM4_ASM})
set(CMAKE_C_RESPONSE_FILE_LINK_FLAG "--via=")
set(CMAKE_CXX_RESPONSE_FILE_LINK_FLAG "--via=")

set(CORTEXM4_FLAGS "-c --cpu Cortex-M4.fp -D__EVAL -g -O0 --apcs=interwork --split_sections -DSTM32F407xx -DSTM32F4XX -DOS_FREERTOS -Dassert_param(expr) -DuPLC -DISOC300 -I${ARM_DIR}/CMSIS/Include")
set(CMAKE_ASM_FLAGS "--cpu Cortex-M4.fp -g --apcs=interwork" CACHE STRING "")
set(CMAKE_C_FLAGS "--c99 -W ${CORTEXM4_FLAGS}" CACHE STRING "")
set(CMAKE_C_FLAGS_RELEASE "" CACHE STRING "")
set(CMAKE_C_FLAGS_DEBUG "" CACHE STRING "")
set(CMAKE_CXX_FLAGS "${CORTEXM4_FLAGS} --cpp" CACHE STRING "")
set(CMAKE_EXE_LINKER_FLAGS "--cpu Cortex-M4.fp --scatter ${LD_SCRIPT_DIR}/EclrMain_FreeRTOS_F4.sct --strict --info=totals --info summarysizes --symbols --info sizes" CACHE STRING "")

#special targets

#add_binary
function(add_binary binary exe)
  set(binaryFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.hex)
  add_custom_command(
    OUTPUT ${binaryFile}
    COMMAND ${CORTEXM4_HEXGEN} --i32 -o ${binaryFile} $<TARGET_FILE:${exe}>
    DEPENDS ${exe}
  )

  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${binaryFile}
  )

  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${binaryFile}
  )
endfunction()

