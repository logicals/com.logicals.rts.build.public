### general toolchain configuration

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/../../../Modules")

set(AVR_PROGRAMMER wiring)
set(CPU_TOOLCHAIN_FILE ATmega2560)
include(${CMAKE_CURRENT_LIST_DIR}/toolchain.Arduino.cmake)

set(RTS_SYSTEM_VARIANT ControllinoMega)
