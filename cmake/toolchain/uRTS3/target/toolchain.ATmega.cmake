# Name of the target platform
set(WIN32 0)
set(CMAKE_SYSTEM_VERSION 1)

include(${CMAKE_CURRENT_LIST_DIR}/../../common/toolchain.env.cmake)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  set(CMAKE_C_COMPILER ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-gcc.exe)
  set(CMAKE_CXX_COMPILER ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-g++.exe)
  set(CMAKE_ASM_COMPILER ${CMAKE_C_COMPILER})
  set(AVR_SIZE ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-size.exe)
  set(AVR_OBJCOPY ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-objcopy.exe)
  set(AVR_NM ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-nm.exe)
  set(AVR_OBJDUMP ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-objdump.exe)
  set(CMAKE_AR ${RTS_ENV_BIN_HOME}/compiler/AVRgcc/avr-gcc-4.9.2-mingw32/bin/avr-gcc-ar.exe CACHE INTERNAL "" FORCE)
  set(AVRDUDE ${RTS_ENV_BIN_HOME}/tools/avrdude/avrdude.exe)
  set(AVRDUDE_CFG ${RTS_ENV_BIN_HOME}/tools/avrdude/avr/etc/avrdude.conf)
  set(AVR_SERIAL_PORT "\\\\.\\COM8" CACHE STRING "")
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CMAKE_C_COMPILER /usr/local/toolchains/avr-gcc/bin/avr-gcc)
  set(CMAKE_CXX_COMPILER /usr/local/toolchains/avr-gcc/bin/avr-g++)
  set(CMAKE_ASM_COMPILER ${CMAKE_C_COMPILER})
  set(AVR_SIZE /usr/local/toolchains/avr-gcc/bin/avr-size)
  set(AVR_OBJCOPY /usr/local/toolchains/avr-gcc/bin/avr-objcopy)
  set(AVR_NM /usr/local/toolchains/avr-gcc/bin/avr-nm)
  set(AVR_OBJDUMP /usr/local/toolchains/avr-gcc/bin/avr-objdump)
  set(CMAKE_AR /usr/local/toolchains/avr-gcc/bin/avr-gcc-ar CACHE INTERNAL "" FORCE)
  find_program(AVRDUDE avrdude)
  set(AVRDUDE_CFG /etc/avrdude.conf)
  set(AVR_SERIAL_PORT /dev/ttyACM0 CACHE STRING "")
else()
  MESSAGE(FATAL_ERROR Unsupported build platform.)
endif()

set(CMAKE_C_FLAGS "-mmcu=${AVR_PROC} -ffunction-sections -fdata-sections -fpack-struct -Os -gdwarf-2 -g -Wall -Wpointer-arith -pipe -Wno-unused-parameter -Wno-unused-variable ${CMAKE_C_FLAGS}" CACHE STRING "")
set(CMAKE_C_FLAGS_RELEASE "" CACHE STRING "")
set(CMAKE_C_FLAGS_DEBUG "" CACHE STRING "")
set(CMAKE_CXX_FLAGS "-mmcu=${AVR_PROC} -g -Os -w -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -MMD -flto ${CMAKE_CXX_FLAGS}" CACHE STRING "")
set(CMAKE_ASM_FLAGS "${CMAKE_C_FLAGS} -x assembler-with-cpp -flto ${CMAKE_ASM_FLAGS}" CACHE STRING "")
set(CMAKE_EXE_LINKER_FLAGS "-mmcu=${AVR_PROC} -gdwarf-2 -Wl,-gc-sections -Wl,-Map=app.map ${CMAKE_EXE_LINKER_FLAGS}" CACHE STRING "")

unset(CMAKE_C_FLAGS)
unset(CMAKE_CXX_FLAGS)
unset(CMAKE_ASM_FLAGS)
unset(CMAKE_EXE_LINKER_FLAGS)

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group")


#special targets

#add_size_info
function(add_size_info targetName exe)
  add_custom_target(${targetName} ALL
    COMMAND ${AVR_SIZE} $<TARGET_FILE:${exe}>
  )
endfunction()

#add_binary
function(add_binary binary exe)
  set(binaryFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.hex)
  add_custom_command(
    OUTPUT ${binaryFile}
    COMMAND ${AVR_OBJCOPY} -O ihex -R .eeprom -R .fuse -R .lock -R .signature $<TARGET_FILE:${exe}> ${binaryFile}
    DEPENDS ${exe}
  )
    
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${binaryFile}
  )
  
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${binaryFile}
  )
endfunction()

# Target upload functions
if(NOT EXISTS ${AVRDUDE})
  message(WARNING "AVRDUDE ${AVRDUDE} not found, cannot upload")
  set(CAN_UPLOAD no)
endif()

if(NOT EXISTS ${AVRDUDE_CFG})
  message(WARNING "AVRDUDE config file ${AVRDUDE_CFG} not found, cannot upload")
  set(CAN_UPLOAD no)
endif()

if(NOT DEFINED AVR_PROGRAMMER)
  message(WARNING "AVR_PROGRAMMER not defined, cannot upload")
  set(CAN_UPLOAD no)
endif()

if(NOT DEFINED AVR_BAUD)
  message(WARNING "AVR_BAUD not defined, cannot upload")
  set(CAN_UPLOAD no)
endif()

if(NOT DEFINED AVR_SERIAL_PORT)
  message(WARNING "AVR_SERIAL_PORT not defined, cannot upload")
  set(CAN_UPLOAD no)
endif()


if(CAN_UPLOAD)
  function(add_target_upload targetName binary)
    add_custom_target(${targetName} ${ARGV2}
      COMMAND ${AVRDUDE} -C${AVRDUDE_CFG} -q -p${AVR_PROC} -c${AVR_PROGRAMMER} -P${AVR_SERIAL_PORT} -b${AVR_BAUD} -D -Uflash:w:$<TARGET_PROPERTY:${binary},BINARY_FILE>:i
      DEPENDS ${binary}
    )
  endfunction()
endif()

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${AVR_OBJCOPY} -D OBJDUMP=${AVR_OBJDUMP} -D NM=${AVR_NM}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=.lcrismd
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()
