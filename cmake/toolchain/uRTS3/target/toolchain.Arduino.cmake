### general toolchain configuration

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/../../../Modules")

set(CMAKE_SYSTEM_NAME Arduino)
set(CMAKE_SYSTEM_VERSION 1)

set(CMAKE_BUILD_TYPE Release CACHE STRING "Default build type for Arduino lib")

set(CMAKE_C_FLAGS "-w -std=gnu11 -MMD -flto -fno-fat-lto-objects")
set(CMAKE_CXX_FLAGS "-std=gnu++11")
set(CMAKE_ASM_FLAGS "${CMAKE_C_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "-w -Os -flto -fuse-linker-plugin")

set(CMAKE_CXX_ARCHIVE_CREATE "<CMAKE_AR> rcs <TARGET> <OBJECTS>")
set(CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> rcs <TARGET> <OBJECTS>")
set(CMAKE_C_ARCHIVE_FINISH "")
set(CMAKE_CXX_ARCHIVE_FINISH "")

set(CAN_UPLOAD yes)
set(AVR_BAUD 115200)
include(${CMAKE_CURRENT_LIST_DIR}/toolchain.${CPU_TOOLCHAIN_FILE}.cmake)

