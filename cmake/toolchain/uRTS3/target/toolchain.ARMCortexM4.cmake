# Name of the target platform
set(WIN32 0)
set(CMAKE_SYSTEM_NAME BareMetalCortexM4)
set(CMAKE_SYSTEM_VERSION 1)

set(LD_SCRIPT_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../../ldscripts)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  set(CORTEXM4_COMPILER ${RTS_ENV_BIN_HOME}/compiler/ARMCortexGCC/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-gcc.exe)
  set(CORTEXM4_SIZE ${RTS_ENV_BIN_HOME}/compiler/ARMCortexGCC/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-size.exe)
  set(CORTEXM4_OBJCOPY ${RTS_ENV_BIN_HOME}/compiler/ARMCortexGCC/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-objcopy.exe)
  set(CORTEXM4_NM ${RTS_ENV_BIN_HOME}/compiler/ARMCortexGCC/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-nm.exe)
  set(CORTEXM4_OBJDUMP ${RTS_ENV_BIN_HOME}/compiler/ARMCortexGCC/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-objdump.exe)
  set(STLINK_CLI ${RTS_ENV_BIN_HOME}/tools/ST-LINK_Utility/ST-LINK_CLI.exe)
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CORTEXM4_COMPILER /usr/local/toolchains/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-gcc)
  set(CORTEXM4_SIZE /usr/local/toolchains/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-size)
  set(CORTEXM4_OBJCOPY /usr/local/toolchains/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-objcopy)
  set(CORTEXM4_NM /usr/local/toolchains/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-nm)
  set(CORTEXM4_OBJDUMP /usr/local/toolchains/gcc-arm-none-eabi-5_3/bin/arm-none-eabi-objdump)
else()
  MESSAGE(FATAL_ERROR Unsupported build platform.)
endif()

set(CMAKE_C_COMPILER ${CORTEXM4_COMPILER})
set(CMAKE_ASM_COMPILER ${CORTEXM4_COMPILER})

set(CORTEXM4_FLAGS "-mcpu=cortex-m4 -mthumb -mabi=aapcs -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra -Wno-unused-parameter -g3 -DDEBUG -D${CORTEXM4_PROC} -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -std=gnu11")
set(CMAKE_ASM_FLAGS "${CORTEXM4_FLAGS} -x assembler-with-cpp" CACHE STRING "")
set(CMAKE_C_FLAGS "${CORTEXM4_FLAGS}" CACHE STRING "")
set(CMAKE_C_FLAGS_RELEASE "" CACHE STRING "")
set(CMAKE_C_FLAGS_DEBUG "" CACHE STRING "")
set(CMAKE_EXE_LINKER_FLAGS "-T ${LD_SCRIPT_DIR}/${CORTEXM4_LDSCRIPT} -Xlinker --gc-sections -Wl,-Map=app.map --specs=nano.specs -lc" CACHE STRING "")

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> -Wl,--whole-archive  <LINK_LIBRARIES> -Wl,--no-whole-archive -Wl,--end-group <CMAKE_C_LINK_FLAGS> <LINK_FLAGS>")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> -Wl,--whole-archive <LINK_LIBRARIES> -Wl,--no-whole-archive -Wl,--end-group <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS>")

set(INSTALL_LDSCRIPT "${LD_SCRIPT_DIR}/${CORTEXM4_LDSCRIPT}" CACHE INTERNAL "")

#special targets

#add_size_info
function(add_size_info targetName exe)
  add_custom_target(${targetName} ALL
    COMMAND ${CORTEXM4_SIZE} $<TARGET_FILE:${exe}>
  )
endfunction()

#add_binary
function(add_binary binary exe)
  set(binaryFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.${CORTEXM4_BIN_EXT})
  add_custom_command(
    OUTPUT ${binaryFile}
    COMMAND ${CORTEXM4_OBJCOPY} -O ${CORTEXM4_BIN_FMT} $<TARGET_FILE:${exe}> ${binaryFile}
    DEPENDS ${exe}
  )

  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${binaryFile}
  )

  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${binaryFile}
  )
endfunction()

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${CORTEXM4_OBJCOPY} -D OBJDUMP=${CORTEXM4_OBJDUMP} -D NM=${CORTEXM4_NM}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=.rodata
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()

#add_target_upload
set(CAN_UPLOAD yes)

if(NOT EXISTS ${STLINK_CLI})
  message(WARNING "ST-LINK utility ${STLINK_CLI} not found, cannot upload")
  set(CAN_UPLOAD no)
endif()

if(CAN_UPLOAD)
  function(add_target_upload targetName binary)
    add_custom_target(${targetName} ${ARGV2}
      COMMAND ${STLINK_CLI} -P $<TARGET_PROPERTY:${binary},BINARY_FILE> -rst
      DEPENDS ${binary}
    )
  endfunction()
endif()
