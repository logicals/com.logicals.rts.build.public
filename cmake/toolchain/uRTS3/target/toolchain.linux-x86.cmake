# Name of the target platform
set(WIN32 0)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CMAKE_C_COMPILER gcc)
  set(LINUX_OBJCOPY objcopy)
  set(LINUX_NM nm)
  set(LINUX_OBJDUMP objdump)
else()
  set(CMAKE_C_COMPILER ${RTS_ENV_BIN_HOME}/compiler/i686-none-linux-gnu/bin/i686-none-linux-gnu-gcc.exe)
  set(LINUX_OBJCOPY ${RTS_ENV_BIN_HOME}/compiler/i686-none-linux-gnu/bin/i686-none-linux-gnu-objcopy.exe)
  set(LINUX_NM ${RTS_ENV_BIN_HOME}/compiler/i686-none-linux-gnu/bin/i686-none-linux-gnu-nm.exe)
  set(LINUX_OBJDUMP ${RTS_ENV_BIN_HOME}/compiler/i686-none-linux-gnu/bin/i686-none-linux-gnu-objdump.exe)
endif()

set(CMAKE_C_FLAGS "-m32 -D__I686__ -std=c99 -pedantic -Wall -Wpointer-arith -pipe -Wno-unused-parameter -Wno-unused-variable" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_RELEASE "-O2" CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS_DEBUG "-O0 -g" CACHE STRING "" FORCE)

set(CMAKE_EXE_LINKER_FLAGS "-lpthread -ldl" CACHE STRING "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "" CACHE STRING "" FORCE)

set(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_C_LINK_FLAGS> <LINK_FLAGS>")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS>")

#add_md_binary
function(add_md_binary binary exe)
  set(mdFile ${CMAKE_CURRENT_BINARY_DIR}/${binary}.md)
  add_custom_command(
    OUTPUT ${mdFile}
    COMMAND ${CMAKE_COMMAND}
      -D OBJCOPY=${LINUX_OBJCOPY} -D NM=${LINUX_NM} -D OBJDUMP=${LINUX_OBJDUMP}
      -D EXECUTABLE=$<TARGET_FILE:${exe}> -D MD_FILE=${mdFile}
      -D SEGMENT=.rodata
      -P ${RTS_ENV_HOME}/cmake/scripts/RIS/GccMdGen.cmake
    DEPENDS ${exe}
  )
  add_custom_target(${binary} ${ARGV2}
    COMMAND
    DEPENDS ${mdFile}
  )
  set_property(TARGET ${binary}
    PROPERTY BINARY_FILE ${mdFile}
  )
endfunction()
