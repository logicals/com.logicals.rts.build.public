if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
#hide sh.exe program
    set(CMAKE_SH CMAKE_SH-NOTFOUND CACHE FILEPATH "" FORCE)
    set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)

    #import RTS environment variables
    find_path(RTS_ENV_BIN_HOME "." $ENV{RTS3_HOME} NO_DEFAULT_PATH)
    string(REPLACE "\\" "/" RTS3_COMPILER_HOME $ENV{RTS3_COMPILER_HOME})

    # as there is no compiler in path on Windows the mingw32 compiler will be used by default
    set(CMAKE_C_COMPILER "${RTS3_COMPILER_HOME}/mingw32/bin/gcc.exe")
endif()

set(RTS_BUILD_ENV_DIR ${CMAKE_CURRENT_LIST_DIR}/../../.. CACHE STRING "")
