include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# path to compiler and utilities
set(METZ_EWIO9180_DIR_BASE /usr/local/toolchains/gcc-4.5.3-glibc-2.9-OABI/usr)
set(METZ_EWIO9180_DIR_COMPILER ${METZ_EWIO9180_DIR_BASE}/bin)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# specify the cross compiler
set(CMAKE_C_COMPILER ${METZ_EWIO9180_DIR_COMPILER}/arm-unknown-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${METZ_EWIO9180_DIR_COMPILER}/arm-unknown-linux-gnu-g++)

# compiler flags
set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
