include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Windows)

# specify the cross compiler
if (CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
    set(CMAKE_C_COMPILER i686-w64-mingw32-gcc)
    set(CMAKE_CXX_COMPILER i686-w64-mingw32-g++)
    set(CMAKE_RC_COMPILER_INIT i686-w64-mingw32-windres)
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
    string(REPLACE "\\" "/" RTS3_COMPILER_HOME $ENV{RTS3_COMPILER_HOME})

    set(CMAKE_C_COMPILER "${RTS3_COMPILER_HOME}/mingw32/bin/gcc.exe")
    set(CMAKE_CXX_COMPILER "${RTS3_COMPILER_HOME}/mingw32/bin/g++.exe")
    set(CMAKE_RC_COMPILER_INIT "${RTS3_COMPILER_HOME}/mingw32/bin/windres.exe")
    set(RTS_DEBUGGER "${RTS3_COMPILER_HOME}/mingw32/bin/gdb.exe")
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()

set(CMAKE_RC_COMPILER_ENV_VAR "RC")
set(CMAKE_RC_COMPILER "")
set($ENV{RC} "")

# compiler/linker flags
set(CMAKE_C_FLAGS
    "--std=gnu99 -Werror=implicit-function-declaration"
    CACHE STRING "The compiler flags for compiling C sources" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS
    "-Wl,--enable-stdcall-fixup -static-libgcc -static"
    CACHE STRING "Additional compiler flags for building shared libraries" FORCE)
set(CMAKE_EXE_LINKER_FLAGS
    "-Wl,--enable-stdcall-fixup -static-libgcc -static"
    CACHE STRING "Linker flags to be used to create executables" FORCE)
