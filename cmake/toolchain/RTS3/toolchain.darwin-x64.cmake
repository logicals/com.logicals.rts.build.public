include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Darwin)

# specify the cross compiler
set(CMAKE_C_COMPILER gcc)

# compiler flags
set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration -I${CMAKE_BINARY_DIR}" CACHE STRING "" FORCE)
