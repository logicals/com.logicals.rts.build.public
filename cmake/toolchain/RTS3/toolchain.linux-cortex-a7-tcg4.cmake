include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(DIR_COMPILER /usr/local/toolchains/gcc-linaro-7.2.1-2017.11-x86_64_arm-linux-gnueabihf)
  set(CMAKE_C_COMPILER ${DIR_COMPILER}/bin/arm-linux-gnueabihf-gcc)
  set(CMAKE_CXX_COMPILER ${DIR_COMPILER}/bin/arm-linux-gnueabihf-g++)
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  string(REPLACE "\\" "/" RTS3_COMPILER_HOME $ENV{RTS3_COMPILER_HOME})
  set(DIR_COMPILER ${RTS3_COMPILER_HOME}/gcc-linaro-7.2.1-2017.11-i686-mingw32_arm-linux-gnueabihf)
  set(CMAKE_C_COMPILER ${DIR_COMPILER}/bin/arm-linux-gnueabihf-gcc.exe)
  set(CMAKE_CXX_COMPILER ${DIR_COMPILER}/bin/arm-linux-gnueabihf-g++.exe)
else()
  message(FATAL_ERROR Unsupported build platform.)
endif()

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# compiler flags
set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration -march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7" CACHE STRING "" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--no-undefined" CACHE STRING "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS "-Wl,--no-undefined" CACHE STRING "" FORCE)
