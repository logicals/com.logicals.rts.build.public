include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CMAKE_C_COMPILER clang)
  set(CMAKE_CXX_COMPILER clang++)
else()
  message(FATAL_ERROR "Unsupported host build system.")
endif()

# compiler flags
set(CMAKE_C_FLAGS "-m32 -std=gnu99 -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS "-m32 -std=gnu++11" CACHE STRING "" FORCE)

