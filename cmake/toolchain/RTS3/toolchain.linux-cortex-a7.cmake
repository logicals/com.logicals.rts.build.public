include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# path to compiler and utilities
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(DIR_COMPILER /usr/local/toolchains/gcc-wiba-arm-linux-gnueabihf-4.8)
  set(COMPILER_BIN ${DIR_COMPILER}/usr/bin/cortexa7hf-vfp-neon-fsl-linux-gnueabi/arm-fsl-linux-gnueabi-gcc)
  set(CXX_COMPILER_BIN ${DIR_COMPILER}/usr/bin/cortexa7hf-vfp-neon-fsl-linux-gnueabi/arm-fsl-linux-gnueabi-g++)
  set(CMAKE_SYSROOT ${DIR_COMPILER}/sysroot)
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  file(TO_CMAKE_PATH $ENV{RTS3_COMPILER_HOME} RTS3_COMPILER_HOME)

  set(DIR_COMPILER ${RTS3_COMPILER_HOME}/arm-wittmann-linux-gnueabihf)
  set(COMPILER_BIN ${DIR_COMPILER}/bin/arm-wittmann-linux-gnueabihf-gcc.exe)
  set(CXX_COMPILER_BIN ${DIR_COMPILER}/bin/arm-wittmann-linux-gnueabihf-g++.exe)
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# specify the cross compiler
set(CMAKE_C_COMPILER ${COMPILER_BIN})
set(CMAKE_CXX_COMPILER ${CXX_COMPILER_BIN})

# compiler flags
set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration -march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7" CACHE STRING "" FORCE)

set(CMAKE_CXX_CREATE_SHARED_LIBRARY "<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> -Wl,--start-group <OBJECTS> <LINK_LIBRARIES> -Wl,--end-group -o <TARGET>")
