# Name of the target platform
set(CMAKE_SYSTEM_NAME Windows)

# Specify MSVC compiler
set(CMAKE_C_COMPILER cl)
set(CMAKE_CXX_COMPILER cl)

# Use RTOS32 include and library directories, but don't build the RTS loader
set(RTOS32 1)
set(RTOS32ECWIN 1)
