include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# path to compiler and utilities
set(I586_IOT2000_DIR_BASE /usr/local/toolchains/iot2000/sysroots)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/bin)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/sbin)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/bin)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/sbin)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/x86_64-pokysdk-linux/bin)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/bin/i586-poky-linux)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/bin/i586-poky-linux-uclibc)
list(APPEND CMAKE_PROGRAM_PATH ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/bin/i586-poky-linux-musl)
set(I586_IOT2000_DIR_COMPILER ${I586_IOT2000_DIR_BASE}/x86_64-pokysdk-linux/usr/bin/i586-poky-linux)

# Sysroot for IOT2000
set(CMAKE_SYSROOT ${I586_IOT2000_DIR_BASE}/i586-nlp-32-poky-linux)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
set(CMAKE_C_COMPILER ${I586_IOT2000_DIR_COMPILER}/i586-poky-linux-gcc)
set(CMAKE_CXX_COMPILER ${I586_IOT2000_DIR_COMPILER}/i586-poky-linux-g++)

# compiler flags
set(CMAKE_C_FLAGS "-m32 -march=i586 -Wa,-momit-lock-prefix=yes --std=gnu99 -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS "-m32 -march=i586 -Wa,-momit-lock-prefix=yes -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
