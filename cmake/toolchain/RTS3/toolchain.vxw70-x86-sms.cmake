# the following line is a workaround to make VxWorks compilers play nice with CMake; please do not remove
set(WIN32 0)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/../../Modules")

include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# Name of the target platform
set(CMAKE_SYSTEM_NAME VxWorks)

# Version of the system
set(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
    string(REPLACE "\\" "/" RTS3_COMPILER_HOME $ENV{RTS3_COMPILER_HOME})
    set(RTS3_COMPILER_SCRIPT_DIR "${RTS3_COMPILER_HOME}/../compilerscript/Wrswrb4_70_SR0540/gnu/gnu-4.8.1.10/x86-win32/bin")

    set(CMAKE_C_COMPILER "${RTS3_COMPILER_SCRIPT_DIR}/ccpentium.bat")
    set(CMAKE_CXX_COMPILER "${RTS3_COMPILER_SCRIPT_DIR}/c++pentium.bat")

    set(TARGET_H_DIR "${RTS3_COMPILER_HOME}/Wrswrb4_70_SR0540/Target/krnl/h/public")
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()

# compiler/linker flags
set(CMAKE_C_FLAGS
    "--std=gnu99 -Werror=implicit-function-declaration -fno-feature-proxy -O2 -fno-defer-pop -nostdlib -fno-builtin -mno-sse -mno-sse2 -mtune=pentium -march=pentium -ansi -Wall -MD -MP \
    -Wno-strict-aliasing \
    -DCPU=PENTIUM4 -DTOOL_FAMILY=gnu -DTOOL=gnu -D_WRS_KERNEL -D_WRS_CONFIG_SMP \
    -I${TARGET_H_DIR} -I${TARGET_H_DIR}/wrn/coreip -I${TARGET_H_DIR}/wrn/coreip/sys"
    CACHE STRING "The compiler flags for compiling C sources" FORCE
)
set(CMAKE_SHARED_LINKER_FLAGS
    "-nostdlib -r -pipe -Wl,-X -T ${TARGET_H_DIR}/tool/gnu/ldscripts/link.OUT"
    CACHE STRING "Additional compiler flags for building shared libraries" FORCE
)
set(CMAKE_EXE_LINKER_FLAGS
    "-fno-feature-proxy -r -pipe -Wl,-X -T ${TARGET_H_DIR}/tool/gnu/ldscripts/link.OUT"
    CACHE STRING "Linker flags to be used to create executables" FORCE
)

set(CMAKE_C_COMPILE_OBJECT "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> ${CMAKE_C_FLAGS} -o <OBJECT> -c <SOURCE>")
SET(CMAKE_C_CREATE_SHARED_LIBRARY "<CMAKE_C_COMPILER> <LINK_FLAGS> -o <TARGET> <OBJECTS>")
SET(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <LINK_FLAGS> <OBJECTS> -o <TARGET>")
