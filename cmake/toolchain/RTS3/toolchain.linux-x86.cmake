include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(CMAKE_C_COMPILER gcc)
  set(CMAKE_CXX_COMPILER g++)
else()
  set(CMAKE_C_COMPILER ${RTS_ENV_BIN_HOME}/compiler/i686-logicals-linux-gnu/bin/i686-logicals-linux-gnu-gcc.exe)
  set(CMAKE_CXX_COMPILER ${RTS_ENV_BIN_HOME}/compiler/i686-logicals-linux-gnu/bin/i686-logicals-linux-gnu-g++.exe)
endif()
# compiler flags
set(CMAKE_C_FLAGS "-m32 -std=gnu99 -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS "-m32 -std=gnu++11" CACHE STRING "" FORCE)
