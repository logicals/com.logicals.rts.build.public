include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(ARM9_EABI_DIR_BASE /usr/local/toolchains/gcc-linaro-4.9-2016.02-x86_64_arm-linux-gnueabi)
  set(ARM9_EABI_DIR_COMPILER ${ARM9_EABI_DIR_BASE}/bin)

  set(CMAKE_C_COMPILER ${ARM9_EABI_DIR_COMPILER}/arm-linux-gnueabi-gcc)
  set(CMAKE_CXX_COMPILER ${ARM9_EABI_DIR_COMPILER}/arm-linux-gnueabi-g++)
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  string(REPLACE "\\" "/" RTS3_COMPILER_HOME $ENV{RTS3_COMPILER_HOME})

  set(ARM9_EABI_DIR_BASE ${RTS3_COMPILER_HOME}/gcc-linaro-4.9-arm-linux-gnueabi)
  set(ARM9_EABI_DIR_COMPILER ${ARM9_EABI_DIR_BASE}/bin)

  set(CMAKE_C_COMPILER ${ARM9_EABI_DIR_COMPILER}/arm-linux-gnueabi-gcc.exe)
  set(CMAKE_CXX_COMPILER ${ARM9_EABI_DIR_COMPILER}/arm-linux-gnueabi-g++.exe)
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# compiler flags
set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
