include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)
set(WIN32 0)

# path to compiler and utilities
set(LD_SCRIPT_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../ldscripts)
set(LD_SCRIPT_NAME dSys.ld)
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(DIR_COMPILER /usr/local/toolchains/gcc-linaro-6.2.1-2016.11-x86_64_arm-eabi)
  set(EXT_COMPILER "")
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  file(TO_CMAKE_PATH $ENV{RTS3_COMPILER_HOME} RTS3_COMPILER_HOME)
  set(DIR_COMPILER ${RTS3_COMPILER_HOME}/arm-eabi-gcc6.2.0-r3)
  set(EXT_COMPILER ".exe")
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()
set(DIR_COMPILER_BIN ${DIR_COMPILER}/bin)

# Name of the target platform
set(CMAKE_SYSTEM_NAME dSys)
set(CMAKE_SYSTEM_PROCESSOR arm)

# specify the cross compiler
set(CMAKE_C_COMPILER ${DIR_COMPILER_BIN}/arm-eabi-gcc${EXT_COMPILER})
set(CMAKE_CXX_COMPILER ${DIR_COMPILER_BIN}/arm-eabi-g++${EXT_COMPILER})

# compiler flags
SET(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration -D__DEBUGGING -D__COMPILER_GCC_ARM_V7 -D__TARGET_ARM_V7_STM32F429A439 -mlong-calls -ffunction-sections -fdata-sections -march=armv7e-m -mtune=cortex-m4 -mcpu=cortex-m4 -mabi=aapcs -mthumb -mfloat-abi=soft" CACHE STRING "" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS
    "-mlong-calls -W -Wall -Wstrict-prototypes -Wmissing-prototypes -ffunction-sections -fdata-sections -march=armv7e-m -mtune=cortex-m4 -mcpu=cortex-m4 -mabi=aapcs -mthumb -mfloat-abi=soft -Os -g -shared -fPIC -nostartfiles -Wl,-z,max-page-size=512,-gc-sections,-L${LD_SCRIPT_DIR},-T${LD_SCRIPT_NAME}"
    CACHE STRING "Additional compiler flags for building shared libraries" FORCE
)
set(CMAKE_EXE_LINKER_FLAGS
    "-Wl,--unresolved-symbols=ignore-all"
    CACHE STRING "Linker flags to be used to create executables" FORCE
)

set(CMAKE_C_COMPILE_OBJECT "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> ${CMAKE_C_FLAGS} -o <OBJECT> -c <SOURCE>")
SET(CMAKE_C_CREATE_SHARED_LIBRARY "<CMAKE_C_COMPILER> <LINK_FLAGS> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>")
SET(CMAKE_C_LINK_EXECUTABLE "<CMAKE_C_COMPILER> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>")
