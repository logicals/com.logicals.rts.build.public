# Name of the target platform
set(CMAKE_SYSTEM_NAME Windows)

# Specify MSVC compiler
set(CMAKE_C_COMPILER cl)
set(CMAKE_CXX_COMPILER cl)

# Use RTOS32 include and library directories
set(RTOS32 1)
