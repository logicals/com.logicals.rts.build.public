include(${CMAKE_CURRENT_LIST_DIR}/../common/toolchain.env.cmake)

# path to compiler and utilities
if(CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
  set(DIR_COMPILER /usr/local/toolchains/gcc-linaro-arm-linux-gnueabihf-4.9)
  set(EXT_COMPILER "")
elseif(CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
  file(TO_CMAKE_PATH $ENV{RTS3_COMPILER_HOME} RTS3_COMPILER_HOME)
  set(DIR_COMPILER ${RTS3_COMPILER_HOME}/gcc-linaro-arm-linux-gnueabihf-4.9-2014.09_win32)
  set(EXT_COMPILER ".exe")
else()
    message(FATAL_ERROR Unsupported build platform.)
endif()
set(DIR_COMPILER_BIN ${DIR_COMPILER}/bin)

# Name of the target platform
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# specify the cross compiler
set(CMAKE_C_COMPILER ${DIR_COMPILER_BIN}/arm-linux-gnueabihf-gcc${EXT_COMPILER})
set(CMAKE_CXX_COMPILER ${DIR_COMPILER_BIN}/arm-linux-gnueabihf-g++${EXT_COMPILER})

# compiler flags
set(CMAKE_C_FLAGS "--std=gnu99 -Werror=implicit-function-declaration" CACHE STRING "" FORCE)
