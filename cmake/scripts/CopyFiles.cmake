if(NOT DEFINED DST_DIR)
  message(FATAL_ERROR "DST_DIR is not defined")
endif()

if(NOT DEFINED SRC_DIRS)
  message(FATAL_ERROR "SRC_DIRS is not defined")
endif()

if(NOT DEFINED FILE_FILTER)
  message(FATAL_ERROR "FILE_FILTER is not defined")
endif()

string(REPLACE "@" ";" SRC_DIRS ${SRC_DIRS})  #convert to list

message("copy ${FILE_FILTER} from ${SRC_DIRS}")

foreach(srcDir ${SRC_DIRS})
  file(GLOB srcFiles "${srcDir}/${FILE_FILTER}")
  foreach(srcFile ${srcFiles})
    if(NOT IS_DIRECTORY ${srcFile})
      file(COPY ${srcFile} DESTINATION  ${DST_DIR})
    endif()
  endforeach()
endforeach()



#execute_process(
#  COMMAND ${NM} ${EXECUTABLE}
#  RESULT_VARIABLE nmResult
#  OUTPUT_VARIABLE nmOutput
#)
