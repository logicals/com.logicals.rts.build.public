if(NOT DEFINED EXECUTABLE)
  message(FATAL_ERROR "EXECUTABLE is not defined")
endif()

if(NOT DEFINED MD_FILE)
  message(FATAL_ERROR "MD_FILE is not defined")
endif()

if(NOT DEFINED OBJCOPY)
  message(FATAL_ERROR "OBJCOPY is not defined")
endif()

if(NOT DEFINED OBJDUMP)
  message(FATAL_ERROR "OBJDUMP is not defined")
endif()

if(NOT DEFINED NM)
  message(FATAL_ERROR "NM is not defined")
endif()

if(NOT DEFINED SEGMENT)
  message(FATAL_ERROR "SEGMENT is not defined")
endif()

set(objCopyFile "${MD_FILE}.tmp")

execute_process(
  COMMAND ${OBJCOPY} -O binary -j ${SEGMENT} ${EXECUTABLE} ${objCopyFile}
  RESULT_VARIABLE objCopyResult
  OUTPUT_VARIABLE objCopyOutput
)
execute_process(
  COMMAND ${OBJDUMP} -h ${EXECUTABLE}
  RESULT_VARIABLE objDumpResult
  OUTPUT_VARIABLE objDumpOutput
)
execute_process(
  COMMAND ${NM} ${EXECUTABLE}
  RESULT_VARIABLE nmResult
  OUTPUT_VARIABLE nmOutput
)

message("----- objDump -----")
message("${objDumpOutput}")
message("----- nm -----")
message("${nmOutput}")

string(REGEX MATCH "${SEGMENT}[ ]+[0-9a-f]+[ ]+[0-9a-f]+" sectionAddr ${objDumpOutput})
string(REGEX MATCH "[0-9a-f]+$" sectionAddr ${sectionAddr})

string(REGEX MATCH "[0-9a-f]+[ ]+.[ ]+_?risMdRoot" mdRootAddr ${nmOutput})
string(REGEX MATCH "[0-9a-f]+" mdRootAddr ${mdRootAddr})

string(REGEX MATCH "[0-9a-f]+[ ]+.[ ]+_?risMdTargetInfo" mdTargetInfoAddr ${nmOutput})
string(REGEX MATCH "[0-9a-f]+" mdTargetInfoAddr ${mdTargetInfoAddr})

set(hdrFile "${MD_FILE}.hdr")
file(WRITE ${hdrFile}
  "Metadata file\n"
  "Copyright (C) logi.cals GmbH. All rights reserved.\n"
  "Hdr line cnt:7\n"
  "File format:2\n"
  "Binary base:0x${sectionAddr}\n"
  "MD root:0x${mdRootAddr}\n"
  "MD target info:0x${mdTargetInfoAddr}\n"
)
#merge header and binary
execute_process(
  COMMAND cat ${hdrFile} ${objCopyFile}
  OUTPUT_FILE ${MD_FILE}
  RESULT_VARIABLE genResult
)

