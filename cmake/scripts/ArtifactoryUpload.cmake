if(NOT DEFINED LOCAL_FILE)
    message(FATAL_ERROR "LOCAL_FILE is not defined")
endif()

if(NOT DEFINED ARTIFACTORY_PATH)
    message(FATAL_ERROR "ARTIFACTORY_PATH is not defined")
endif()

file(MD5 ${LOCAL_FILE} md5Sum)
file(SHA1 ${LOCAL_FILE} sha1Sum)

string(REGEX REPLACE "/[^/]+$" "" artifactorDir ${ARTIFACTORY_PATH})

execute_process(
  COMMAND curl -i -X DELETE -u ${ARTIFACTORY_LOGIN} "${artifactorDir}"
  RESULT_VARIABLE deleteResult
  TIMEOUT 20
)
message("delete result = ${deleteResult}")

execute_process(
  COMMAND curl -i -X PUT -u ${ARTIFACTORY_LOGIN} -H "X-Checksum-Md5: ${md5Sum}" -H "X-Checksum-Sha1: ${sha1Sum}" -T "${LOCAL_FILE}" "${ARTIFACTORY_PATH}"
  RESULT_VARIABLE uploadResult
  TIMEOUT 20
)
message("upload result = ${uploadResult}")
