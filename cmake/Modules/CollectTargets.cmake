
# collect include files
# parameters:
#   <collectTarget> ........ name of the variable, where the path is stored
#   <dstDir> ............... destination directory
#   <...> .................. input targets
function(add_include_collection collectTarget dstDir)
  add_custom_target(${collectTarget}_CLEAN
    COMMAND ${CMAKE_COMMAND} -E remove_directory ${dstDir}
  )
  foreach(srcTarget ${ARGN})
    add_custom_target(${collectTarget}_COPY_${srcTarget} ALL
      COMMAND ${CMAKE_COMMAND}
        -D DST_DIR=${dstDir}
        -D SRC_DIRS=$<JOIN:$<TARGET_PROPERTY:${srcTarget},INTERFACE_INCLUDE_DIRECTORIES>,@>
        -D FILE_FILTER=*.h*
        -P ${RTS_ENV_HOME}/cmake/scripts/CopyFiles.cmake
      DEPENDS ${collectTarget}_CLEAN
    )
  endforeach()
endfunction()
