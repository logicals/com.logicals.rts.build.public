# search library and link to target
# parameters:
#   <target> ........ name of the target, where lib is linked to
#   <linkOption>..... link option <PRIVATE|PUBLIC|INTERFACE>
#   <searchDirs> .... input search directories
#   <...> ........... libNames
function(target_link_downloaded_libs target linkOption searchDirs)
  foreach(libName ${ARGN})
    set(libVarName "LIB_VAR_NAME_${libName}")
    find_library(${libVarName} ${libName} 
      PATHS ${searchDirs}
      NO_DEFAULT_PATH
    )
    if(${${libVarName}} MATCHES ".*NOTFOUND" )
      message(FATAL_ERROR "cannot find library ${libName} in ${searchDirs}")
    endif()
    target_link_libraries(${target} ${linkOption} ${${libVarName}})
  endforeach()
endfunction()
