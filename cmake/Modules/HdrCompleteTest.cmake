#templatePath: path to the file serving as template to include given header file
#compileDir: directory where source file is generated
#hdrFilePath: path (directory + name) of header file
#... additional include directories
function(hdrTestCompile templatePath compileDir hdrFilePath)
  get_filename_component(HEADER_FILE ${hdrFilePath} NAME)
  configure_file(${hdrFilePath} ${compileDir}/${HEADER_FILE} COPYONLY)
  set(srcFileUsingHdr ${compileDir}/${HEADER_FILE}.c)
  configure_file(${templatePath} ${srcFileUsingHdr})
  get_filename_component(targetUsingHdr ${hdrFilePath} NAME_WE)
  get_filename_component(hdrDir ${hdrFilePath} DIRECTORY)
  add_library(${targetUsingHdr} STATIC ${srcFileUsingHdr})
  target_include_directories(${targetUsingHdr} PRIVATE ${hdrDir} ${ARGN})
endfunction()

