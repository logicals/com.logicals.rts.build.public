function(target_link_obj_libraries target)
  foreach(objLib ${ARGN})
    target_sources(${target} PRIVATE $<TARGET_OBJECTS:${objLib}>)
  endforeach()
  target_import_include_dirs(${target} PUBLIC ${ARGN})
endfunction()

function(add_library_with_obj_libs target libType firstObjLib)
  add_library(${target} ${libType}
    $<TARGET_OBJECTS:${firstObjLib}>
  )
  target_import_include_dirs(${target} PUBLIC ${firstObjLib})
  target_link_obj_libraries(${target} ${ARGN})
endfunction()

function(add_executable_with_obj_libs target firstObjLib)
  add_executable(${target}
    $<TARGET_OBJECTS:${firstObjLib}>
  )
  target_import_include_dirs(${target} PUBLIC ${firstObjLib})
  target_link_obj_libraries(${target} ${ARGN})
endfunction()


function(target_import_include_dirs target visibility)
  foreach(otherTarget ${ARGN})
    target_include_directories(${target} ${visibility} $<TARGET_PROPERTY:${otherTarget},INTERFACE_INCLUDE_DIRECTORIES>)
  endforeach()
endfunction()

function(target_import_all_include_dirs target visibility)
  foreach(otherTarget ${ARGN})
    target_include_directories(${target} ${visibility} $<TARGET_PROPERTY:${otherTarget},INCLUDE_DIRECTORIES>)
  endforeach()
endfunction()
