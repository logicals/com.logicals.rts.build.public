# build RTS distribution file name
# parameters:
#   <resultVariable> ........... name of the variable, where the filename is stored
#   <targetPlatform> ........... target platform this RTS is built for
#   <targetVendor> ............. vendor of this platform
#   <prototype> ................ true if it is a prototypical version
#   <additional> ............... RTS additional information
function(build_rts_dist_file_name resultVariable targetPlatform targetVendor additional)
    if (NOT DEFINED targetPlatform OR targetPlatform STREQUAL "")
        message(FATAL_ERROR "targetPlatform not provided to build_rts_dist_file_name")
    endif()
    set(result RTS3-${targetPlatform})
    if (NOT targetVendor STREQUAL "")
        set(result ${result}-${targetVendor})
    endif()
    if (NOT additional STREQUAL "")
        set(result ${result}${additional})
    endif()
    set(${resultVariable} ${result} PARENT_SCOPE)
endfunction()
