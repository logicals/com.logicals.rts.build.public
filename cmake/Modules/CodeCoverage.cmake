function (append_coverage_flags)
  if (CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_GNUCC)
      if (NOT WIN32 AND NOT UNIX)
        message(STATUS "Determining code coverage is only supported on Windows and Unix-based systems.")
        return ()
      endif ()
      find_program(GCOV_PATH gcov)
      if ((CMAKE_BUILD_TYPE STREQUAL "Debug") OR (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo"))
        if (GCOV_PATH)
          link_libraries(gcov)
          add_compile_options(-fprofile-arcs -ftest-coverage)
        else ()
          message(STATUS "Cannot find coverage tools. Skipping analysis.")
        endif ()
      else ()
        message(STATUS "Code coverage is only determined in Debug or RelWithDebInfo builds.")
      endif ()
  endif ()
endfunction () # append_coverage_flags

function (add_target_coverage_report)
  if (NOT WIN32 AND NOT UNIX)
    return ()
  endif ()
  find_program(GCOVR_PATH gcovr)
  if (NOT GCOVR_PATH)
    message(STATUS "Gcovr not found. Not adding a code coverage report target.")
    add_custom_target(coverage_report)
  else ()
    set(outputDir ${PROJECT_BINARY_DIR}/coverage_report)
    if (NOT EXISTS ${outputDir})
      file(MAKE_DIRECTORY ${outputDir})
    endif ()
    add_custom_target(coverage_report
      COMMAND ${GCOVR_PATH} --html --html-details -r ${PROJECT_SOURCE_DIR} --object-directory=${PROJECT_BINARY_DIR} -o ${outputDir}/index.html
      WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
      DEPENDS test
    )
  endif ()
endfunction ()


