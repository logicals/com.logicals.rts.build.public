# http://stackoverflow.com/questions/9303711/how-do-i-make-ctest-run-a-program-with-valgrind-without-dart

find_program(MEMORYCHECK_COMMAND valgrind)
if(NOT MEMORYCHECK_COMMAND)
    message(STATUS "Cannot find memory checker on your host platform. Executing tests without it.")
endif()

set(MEMORYCHECK_COMMAND_OPTIONS "--tool=memcheck \
--leak-check=full \
--show-reachable=no \
--undef-value-errors=no \
--track-origins=no \
--child-silent-after-fork=no \
--trace-children=yes \
--gen-suppressions=no \
--xml=yes \
--xml-file=" CACHE STRING "Options for the memory check program")

function(_get_environment_for_test resultVar varList)
    if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
        set(TEST_LIB_PATHS_WINDOWS "")
        foreach(_path ${varList})
            set(TEST_LIB_PATHS_WINDOWS "${_path}\;${TEST_LIB_PATHS_WINDOWS}")
        endforeach()
        set(${resultVar} "PATH=${TEST_LIB_PATHS_WINDOWS}" PARENT_SCOPE)
    else ()
        set(TEST_LIB_PATHS_UNIX "")
        foreach(_path ${varList})
            set(TEST_LIB_PATHS_UNIX "${_path}:${TEST_LIB_PATHS_UNIX}")
        endforeach()
        if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
            set(${resultVar} "LD_LIBRARY_PATH=${TEST_LIB_PATHS_UNIX}" PARENT_SCOPE)
        elseif (CMAKE_SYSTEM_NAME STREQUAL "Darwin")
            set(${resultVar} "DYLD_LIBRARY_PATH=${TEST_LIB_PATHS_UNIX}" PARENT_SCOPE)
        endif ()
    endif ()
endfunction ()

function(add_memcheck_test name binary)
    if(MEMORYCHECK_COMMAND)
        if(NOT EXISTS ${CMAKE_BINARY_DIR}/memcheck)
            file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/memcheck)
        endif()
        set(memcheck_command "${MEMORYCHECK_COMMAND} ${MEMORYCHECK_COMMAND_OPTIONS}${CMAKE_BINARY_DIR}/memcheck/${name}.memcheck")
        separate_arguments(memcheck_command)
        if (TARGET ${binary})
          add_test(NAME "${name}_memchecked" COMMAND ${memcheck_command} $<SHELL_PATH:$<TARGET_FILE:${binary}>> ${ARGN})
          add_test(NAME "${name}_unchecked" COMMAND $<SHELL_PATH:$<TARGET_FILE:${binary}>> ${ARGN})
        else ()
          add_test(NAME "${name}_memchecked" COMMAND ${memcheck_command} ${binary} ${ARGN})
          add_test(NAME "${name}_unchecked" COMMAND ${binary} ${ARGN})
        endif ()
    else()
        if (TARGET ${binary})
          add_test(NAME ${name} COMMAND $<SHELL_PATH:$<TARGET_FILE:${binary}>> ${ARGN})
        else ()
          add_test(NAME ${name} COMMAND ${binary} ${ARGN})
        endif ()
    endif()
endfunction(add_memcheck_test)

function(set_memcheck_test_env name)
    _get_environment_for_test(testEnv "${ARGN}")
    if(MEMORYCHECK_COMMAND)
      set_property(TEST "${name}_memchecked" PROPERTY ENVIRONMENT "${testEnv}")
      set_property(TEST "${name}_unchecked" PROPERTY ENVIRONMENT "${testEnv}")
    else ()
      set_property(TEST ${name} PROPERTY ENVIRONMENT "${testEnv}")
    endif ()
endfunction ()

function(set_memcheck_test_property name propertyName valueMemchecked valueUnchecked)
    if(MEMORYCHECK_COMMAND)
      set_property(TEST "${name}_memchecked" PROPERTY "${propertyName}" "${valueMemchecked}")
      set_property(TEST "${name}_unchecked" PROPERTY "${propertyName}" "${valueUnchecked}")
    else ()
      set_property(TEST ${name} PROPERTY "${propertyName}" "${valueUnchecked}")
    endif ()
endfunction ()

