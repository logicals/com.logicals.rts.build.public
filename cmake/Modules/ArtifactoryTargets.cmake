include(HTTPTargets)

set(ARTIFACTORY_URL "https://artifactory.logicals.com/artifactory" CACHE STRING "")
set(ARTIFACTORY_LOGIN $ENV{ARTIFACTORY_LOGIN} CACHE STRING "")

if(NOT ARTIFACTORY_LOGIN)
  message(STATUS "ARTIFACTORY_LOGIN is empty. Only local builds are possible.")
  function(build_repository_path resultVariable groupId artifactId version classifier type)
    set(${resultVariable} "Not calculated because ARTIFACTORY_LOGIN is empty." PARENT_SCOPE)
  endfunction()
  function(build_repository_path_full resultVariable groupId artifactId version classifier type repositoryPrefix)
    set(${resultVariable} "Not calculated because ARTIFACTORY_LOGIN is empty." PARENT_SCOPE)
  endfunction()
  function(add_artifactory_download artifactFile repositoryPath)
  endfunction()
  function(add_artifactory_file_upload targetName fileName repositoryPath)
  endfunction()
  return()
endif()

# build repository path
# parameters:
#   <resultVariable> ........ name of the variable, where the path is stored
#   <groupId> ............... artifact group id - e.g. com.logicals.ptk
#   <artifactId> ............ artifact id - e.g. RTSAPI
#   <version> ............... artifact version - e.g. 0.0.1
#   <classifier> ............ artifact classifier - e.g. uRTS3
#   <type> .................. artifact type - e.g. zip
function(build_repository_path resultVariable groupId artifactId version classifier type)
  build_repository_path_full(repoPath "${groupId}" "${artifactId}" "${version}" "${classifier}" "${type}" "libs")
  set(${resultVariable} ${repoPath} PARENT_SCOPE)
endfunction()

function(build_repository_path_full resultVariable groupId artifactId version classifier type repositoryPrefix)
  string(REPLACE "." "/" groupIdAsPath ${groupId})

  string(FIND ${version} "SNAPSHOT" snapshotIdx)
  if(snapshotIdx GREATER 0 OR snapshotIdx EQUAL 0)
    set(repository "${repositoryPrefix}-snapshot-local")
  else()
    set(repository "${repositoryPrefix}-release-local")
  endif()

  if(classifier)
    set(classifierStr "-${classifier}")
  else()
    set(classifierStr "")
  endif()
  set(${resultVariable} "${repository}/${groupIdAsPath}/${artifactId}/${version}/${artifactId}-${version}${classifierStr}.${type}" PARENT_SCOPE)
endfunction()

# download file from artifactory
# parameters:
#   <artifactFile> ...... output file
#   <repositoryPath> .... the repositorypath - e.g. ext-release-local/com/logicals/ptk/lc3/LCStd/2.2.2/LCStd-2.2.2-uRTS3.zip
# provides target attribute BINARY_FILE
function(add_artifactory_download artifactFile repositoryPath)
  set(artifactUrl ${ARTIFACTORY_URL}/simple/${repositoryPath})
  add_http_auth_download(${artifactFile} ${artifactUrl} ${ARTIFACTORY_LOGIN})
endfunction()

# upload zip to artifactory
# parameters:
#   <targetName> ........ name of the target which will perform the upload
#   <fileName> .......... file to upload
#   <repositoryPath> .... the repositorypath - e.g. "ext-release-local/com/logicals/ptk/rts/RTSAPI/0.0.1/RTSAPI-0.0.1-uRTS3.zip"
#   <> .................. ALL, if upload should be done by make all
function(add_artifactory_file_upload targetName fileName repositoryPath)
  if(NOT TARGET deploy)
    add_custom_target(deploy COMMAND)
  endif()

  add_custom_target(${targetName} ${ARGV3}
    COMMAND ${CMAKE_COMMAND} -D ARTIFACTORY_LOGIN=${ARTIFACTORY_LOGIN} -D LOCAL_FILE=${fileName} -D ARTIFACTORY_PATH=${ARTIFACTORY_URL}/${repositoryPath} -P ${RTS_ENV_HOME}/cmake/scripts/ArtifactoryUpload.cmake
    DEPENDS ${zipTarget}
  )
  add_dependencies(deploy ${targetName})
endfunction()
