# This file contains functions and configurations for generating PC-Lint build
# targets for your CMake projects.

#determine standard compiler include path
include(CMakeExtraGeneratorDetermineCompilerMacrosAndIncludeDirs)

set(LINT_INSTALL_PATH "")
find_program(LINT_EXECUTABLE_PATH NAMES lint-nt flint flexelint lint-nt.sh PATHS ${LINT_INSTALL_PATH} ENV PATH)
get_filename_component(LINT_EXECUTABLE ${LINT_EXECUTABLE_PATH} NAME)

if(LINT_EXECUTABLE)
    set(LINT_EXECUTABLE ${LINT_EXECUTABLE_PATH})
    message(STATUS "-- Found Lint: ${LINT_EXECUTABLE_PATH}")

    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/targets)

else(LINT_EXECUTABLE)
    message(WARNING "Lint not found! Not performing static analysis.")
endif(LINT_EXECUTABLE)

set(LINT_DATA_DIR ${CMAKE_SOURCE_DIR}/lint)
set(LINT_CONFIG_DIR ${CMAKE_SOURCE_DIR}/lint)
set(LINT_EXTRA_FLAGS "+fvr")
set(LINT_USER_FLAGS "-b")

# add_lint(target source1 [source2 ...])
#
# Takes target and an optional list of source files and generates a configuration
# that can be used for linting all files
#
# The generated lint commands assume that a top-level config file named
# 'std.lnt' resides in the configuration directory 'LINT_CONFIG_DIR'. This
# config file must include all other config files. This is standard lint
# behaviour.
#
# Configuration files for running PC-lint are only generated in case a
# LINT_EXECUTABLE could be found.
#
# Parameters:
#  - target: the name of the target to which the sources belong. You will get a
#            JSON configuration file ${CMAKE_CURRENT_BINARY_DIR}/target/${target}_lint.json
#            that can be parsed by given Python support code
#            new build target named ${target}_LINT
#  - source1 ... : an optional list of source files to be linted in addition to the targets source files.
#
# Example:
#  If you have a CMakeLists.txt which generates an executable like this:
#
#    set(MAIN_SOURCES main.c foo.c bar.c)
#    add_executable(main ${MAIN_SOURCES})
#
#  include this file
#
#    include(FindPCLint.cmake)
#
#  and add a line to generate the main_LINT target
#
#    add_lint(main)
#
function(add_lint target)
    if (NOT LINT_EXECUTABLE)
        return ()
    endif(NOT LINT_EXECUTABLE)

    get_target_property(target_type ${target} TYPE)

    if(NOT (target_type STREQUAL "INTERFACE_LIBRARY"))

        message(INFO " Adding ${target} to lint check.")
        get_target_property(lint_include_directories ${target} INCLUDE_DIRECTORIES)

        # prepend each include directory with "-i"; also quotes the directory
        set(lint_include_directories_transformed)

        foreach(include_dir ${lint_include_directories} ${CMAKE_EXTRA_GENERATOR_C_SYSTEM_INCLUDE_DIRS})
            string(REPLACE "\\" "/" include_dir_normalized "${include_dir}")
            list(APPEND lint_include_directories_transformed "${include_dir_normalized}")
        endforeach(include_dir)

        set(lint_link_libraries "" CACHE INTERNAL "" FORCE)
        get_link_libraries(lint_link_libraries ${target})

        if(lint_link_libraries)
            foreach(linked_lib ${lint_link_libraries})
                get_target_property(lint_include_library_directories ${linked_lib} INTERFACE_INCLUDE_DIRECTORIES)
                foreach(include_dir ${lint_include_directories} ${lint_include_library_directories})
                    string(REPLACE "\\" "/" include_dir_normalized "${include_dir}")
                    list(APPEND lint_include_directories_transformed "${include_dir_normalized}")
                endforeach(include_dir)
            endforeach(linked_lib)
        endif()

        # prepend each definition with "-d"
        get_directory_property(lint_defines COMPILE_DEFINITIONS)
        set(lint_defines_transformed)
        foreach(definition ${lint_defines})
            list(APPEND lint_defines_transformed ${definition})
        endforeach(definition)

        list(REMOVE_DUPLICATES lint_include_directories_transformed)

        get_target_property(sourcefiles ${target} SOURCES)
        list(APPEND sourcefiles ${ARGN})

        set(lint_sourcefiles_abs)

        foreach(sourcefile ${sourcefiles})
            # only include c and cpp files
            if( sourcefile MATCHES \\.c$|\\.cxx$|\\.cpp$ )
                # make filename absolute
                get_filename_component(sourcefile_abs ${sourcefile} ABSOLUTE)
                list(APPEND lint_sourcefiles_abs ${sourcefile_abs})
            endif()
        endforeach(sourcefile)

        # create command line for linting all source files of the target
        file(GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/target/${target}_lint.json
          CONTENT
          "{
  \"LintExecutable\": \"${LINT_EXECUTABLE}\",
  \"ConfigParams\": \"-i${LINT_CONFIG_DIR};std.lnt\",
  \"UserFlags\": \"${LINT_USER_FLAGS}\",
  \"ExtraFlags\": \"${LINT_EXTRA_FLAGS}\",
  \"IncludeDirs\": \"${lint_include_directories_transformed}\",
  \"Defines\": \"${lint_defines}\",
  \"SourceFiles\": \"${lint_sourcefiles_abs}\"
}")
    endif(NOT (target_type STREQUAL "INTERFACE_LIBRARY"))
endfunction(add_lint)

function(get_link_libraries OUTPUT_LIST TARGET)
    set(LOCAL_OUTPUT_LIST ${${OUTPUT_LIST}})
    list(APPEND LOCAL_OUTPUT_LIST ${TARGET})
    list(REMOVE_DUPLICATES LOCAL_OUTPUT_LIST)
    set(${OUTPUT_LIST} ${LOCAL_OUTPUT_LIST} CACHE INTERNAL "" FORCE)

    get_target_property(LIBS ${TARGET} INTERFACE_LINK_LIBRARIES)

    foreach(LIB ${LIBS})
        if (TARGET ${LIB})
            list(FIND LOCAL_OUTPUT_LIST ${LIB} VISITED)
            if (${VISITED} EQUAL -1) # not found
                get_link_libraries(${OUTPUT_LIST} ${LIB})
            endif()
        endif()
    endforeach()
endfunction(get_link_libraries)
