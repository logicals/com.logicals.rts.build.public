# CMAKE_XXX_LIBRARY_PREFIX cannot be set in toolchain files
if (
        CMAKE_SYSTEM_NAME STREQUAL "Linux" OR
        CMAKE_SYSTEM_NAME STREQUAL "Darwin" OR
        CMAKE_SYSTEM_NAME STREQUAL "dSys"
    )
    set(CMAKE_STATIC_LIBRARY_PREFIX "lib")
    set(CMAKE_SHARED_LIBRARY_PREFIX "lib")
    set(CMAKE_IMPORT_LIBRARY_PREFIX "lib")

    # RTS_LIB_PREFIX is deprecated. Please do not use anymore.
    set(RTS_LIB_PREFIX "lib")
else ()
    set(CMAKE_STATIC_LIBRARY_PREFIX "")
    set(CMAKE_SHARED_LIBRARY_PREFIX "")
    set(CMAKE_IMPORT_LIBRARY_PREFIX "")

    # RTS_LIB_PREFIX is deprecated. Please do not use anymore.
    set(RTS_LIB_PREFIX "")
endif ()


