# download file from HTTP server with authentication
# parameters:
#   <targetFile> ... output file
#   <url> .......... url to download from
#   <auth> ......... user authentication as user:pass
# provides target attribute BINARY_FILE
function(add_http_auth_download targetFile url auth)
  if (EXISTS ${targetFile})
    message(STATUS "Artifact '${targetFile}' from '${url}' already exists")
  else ()
    message(STATUS "Downloading artifact '${artifactUrl}' to '${artifactFile}'")
    get_filename_component(targetDir ${targetFile} DIRECTORY)
    file(MAKE_DIRECTORY ${targetDir})
    execute_process(
      COMMAND curl -u${auth} --create-dirs -o ${targetFile} ${url}
      RESULT_VARIABLE curlResult
    )
    if (NOT curlResult EQUAL 0)
      message(FATAL_ERROR "Could not download file: ${targetFile} from ${url}")
    endif ()
  endif ()
endfunction()


