set(RTS_ENV_HOME ${CMAKE_CURRENT_LIST_DIR}/../..)

include(CommonBuildTargets)
include(ArtifactoryTargets)
include(ZipTargets)
include(CollectTargets)
