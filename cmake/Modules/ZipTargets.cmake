function(prepare_archive_extraction outputDir archiveFile)
  if (NOT EXISTS ${archiveFile})
    message(FATAL_ERROR "${archiveFile} could not be found. Aborting.")
  endif()
  file(MAKE_DIRECTORY ${outputDir})
endfunction()

function(add_extract outputDir archiveFile)
  prepare_archive_extraction(${outputDir} ${archiveFile})
  execute_process(
    WORKING_DIRECTORY ${outputDir}
    COMMAND cmake -E tar xf ${archiveFile}
  )
endfunction ()

function(add_unzip outputDir zipFile)
  prepare_archive_extraction(${outputDir} ${zipFile})
  execute_process(
    COMMAND unzip -qq -o ${zipFile} -d ${outputDir}
  )
endfunction()

function(add_untar outputDir tarFile)
  prepare_archive_extraction(${outputDir} ${tarFile})
  execute_process(
    COMMAND tar -xf ${tarFile} -C ${outputDir}
  )
endfunction()
