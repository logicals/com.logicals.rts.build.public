/*!
   \file
   \brief       Linker script for C applications

   \copyright   Copyright 2019 Sensor-Technik Wiedemann GmbH. All rights reserved.
*/

/* Include platform memories and core interop sections */
INCLUDE tc299_memdef.ld

/* -- Memory partitioning (flash, lmu ram, emem) -------------------------------------------------------------------- */
MEMORY_LAYOUT_IDENTIFIER = 3;                 /* The memory layout identifier must match across all core applications */

MEMORY
{
/* Flash Partitioning */
   core0_pflash          (rx) :   org = 0x80080400, len = 0x0013FC00        /* [0x80080400, 0x801C0000[    (1279 KiB) */

   core1_pflash_stl      (rx) :   org = 0x801C0000, len = 0x00040000        /* [0x801C0000, 0x80200000[     (256 KiB) */
   core1_pflash          (rx) :   org = 0x80200000, len = 0x001FFC00        /* [0x80200000, 0x803FFC00[    (2047 KiB) */

   core2_pflash_seg0     (rx) :   org = 0x80400400, len = 0x001FFC00        /* [0x80400400, 0x80600000[    (2047 KiB) */
   core2_pflash_seg1     (rx) :   org = 0x80600000, len = 0x001FFC00        /* [0x80600000, 0x807FFC00[    (2047 KiB) */

/* LMU Partitioning */
   core0_lmu             (wx) :   org = 0x90000000, len = 0x00000800        /* [0x90000000, 0x90000800[       (2 KiB) */
   core1_lmu             (wx) :   org = 0x90000800, len = 0x00007000        /* [0x90000800, 0x90007800[      (28 KiB) */
   core2_lmu             (wx) :   org = 0x90007800, len = 0x00000800        /* [0x90007800, 0x90008000[       (2 KiB) */

/* EMEM Partitioning */
   core1_emem            (wx) :   org = 0x9F000000, len = 0x00080000        /* [0x9F000000, 0x9F080000[     (512 KiB) */
   core2_emem            (wx) :   org = 0x9F080000, len = 0x00169000        /* [0x9F080000, 0x9F1F0000[    (1444 KiB) */
   core2_emem_eth        (wx) :   org = 0x9F1E9000, len = 0x00007000        /* [0x9F1E9000, 0x00007000[      (28 KiB) */
   core0_emem            (wx) :   org = 0x9F1F0000, len = 0x00010000        /* [0x9F1F0000, 0x9F200000[      (64 KiB) */
}

/* Define uncached counterparts where applicable */
MEMORY
{
   core0_pflash_uc       (rx) :   org = ORIGIN(core0_pflash)      | 0x20000000, len = LENGTH(core0_pflash)
   core1_pflash_uc       (rx) :   org = ORIGIN(core1_pflash)      | 0x20000000, len = LENGTH(core1_pflash)

   core2_pflash_seg0_uc  (rx) :   org = ORIGIN(core2_pflash_seg0) | 0x20000000, len = LENGTH(core2_pflash_seg0)
   core2_pflash_seg1_uc  (rx) :   org = ORIGIN(core2_pflash_seg1) | 0x20000000, len = LENGTH(core2_pflash_seg1)
   core0_lmu_uc          (wx) :   org = ORIGIN(core0_lmu)         | 0x20000000, len = LENGTH(core0_lmu)
   core1_lmu_uc          (wx) :   org = ORIGIN(core1_lmu)         | 0x20000000, len = LENGTH(core1_lmu)
   core2_lmu_uc          (wx) :   org = ORIGIN(core2_lmu)         | 0x20000000, len = LENGTH(core2_lmu)
   core1_emem_uc         (wx) :   org = ORIGIN(core1_emem)        | 0x20000000, len = LENGTH(core1_emem)
   core2_emem_uc         (wx) :   org = ORIGIN(core2_emem)        | 0x20000000, len = LENGTH(core2_emem)
   core2_emem_eth_uc     (wx) :   org = ORIGIN(core2_emem_eth)    | 0x20000000, len = LENGTH(core2_emem_eth)
   core0_emem_uc         (wx) :   org = ORIGIN(core0_emem)        | 0x20000000, len = LENGTH(core0_emem)
}

REGION_MIRROR("core0_pflash_uc",      "core0_pflash")
REGION_MIRROR("core1_pflash_uc",      "core1_pflash")

REGION_MIRROR("core2_pflash_seg0_uc", "core2_pflash_seg0")
REGION_MIRROR("core2_pflash_seg1_uc", "core2_pflash_seg1")
REGION_MIRROR("core0_lmu_uc",         "core0_lmu")
REGION_MIRROR("core1_lmu_uc",         "core1_lmu")
REGION_MIRROR("core2_lmu_uc",         "core2_lmu")
REGION_MIRROR("core0_emem_uc",        "core0_emem")
REGION_MIRROR("core1_emem_uc",        "core1_emem")
REGION_MIRROR("core2_emem_uc",        "core2_emem")

/* -- Map aurix_common_sections_begin.ld and aurix_common_sections_end.ld ------------------------------------------- */
MEM_ISTACK_SIZE  =  4K;                                    /* Interrupt stack size in bytes                           */
MEM_USTACK_SIZE  = 20K;                                    /* User stack size in bytes                                */
MEM_CSA_MIN_SIZE = 24K;                                    /* Minimum size of CSA area in bytes                       */
MEM_LCX_HEADROOM =   6;                                    /* Number of reserved lower contexts for trap handling     */
MEM_USE_SAFETLIB =   1;                                    /* Use SafeTlib (use defaults for dpsr and pspr sizes)     */

REGION_ALIAS("MEM_BOOT_ROM",       core2_boot_rom);        /* Startup code and data placed at static address          */
REGION_ALIAS("MEM_LOAD_ROM",       core2_pflash_seg0);     /* Data/Code load address area                             */
REGION_ALIAS("MEM_PSPR",           core2_pspr);            /* PSPR coupled with this core                             */
REGION_ALIAS("MEM_DSPR",           core2_dspr);            /* DSPR coupled with this core                             */
REGION_ALIAS("MEM_LMU",            core2_lmu);             /* LMU SRAM area assigned to this core                     */
REGION_ALIAS("MEM_LMU_UC",         core2_lmu_uc);          /* LMU SRAM area assigned to this core (uncached)          */
REGION_ALIAS("MEM_SDATA_BIOS_RAM", core2_lmu);             /* Small data for internal usage                           */
REGION_ALIAS("MEM_SDATA_BIOS_ROM", core2_pflash_seg0);     /* Small rom for internal usage                            */
REGION_ALIAS("MEM_SDATA_USER_RAM", core2_emem);            /* Small ram data reserved for user                        */
REGION_ALIAS("MEM_SDATA_USER_ROM", core2_pflash_seg0);     /* Small rom reserved for user                             */

REGION_ALIAS("MEM_DEFAULT_ROM_0",  core2_pflash_seg0);     /* Application default .text and .rodata section           */
REGION_ALIAS("MEM_DEFAULT_ROM_1",  core2_pflash_seg1);     /* Application default .text and .rodata (continued)       */
REGION_ALIAS("MEM_DEFAULT_ROM_2",  no_mem);                /* Application default .text and .rodata (continued)       */
REGION_ALIAS("MEM_DEFAULT_ROM_3",  no_mem);                /* Application default .text and .rodata (continued)       */
REGION_ALIAS("MEM_DEFAULT_ROM_4",  no_mem);                /* Application default .text and .rodata (continued)       */

REGION_ALIAS("MEM_DEFAULT_RAM_0",  core2_emem);            /* Application default .data and .bss section              */
REGION_ALIAS("MEM_DEFAULT_RAM_1",  no_mem);                /* Application default .data and .bss section (continued)  */
REGION_ALIAS("MEM_DEFAULT_RAM_2",  no_mem);                /* Application default .data and .bss section (continued)  */
REGION_ALIAS("MEM_DEFAULT_RAM_3",  no_mem);                /* Application default .data and .bss section (continued)  */
REGION_ALIAS("MEM_DEFAULT_RAM_4",  no_mem);                /* Application default .data and .bss section (continued)  */

/* System Heap */
MEM_SYSTEM_HEAP_MIN_SIZE = 10K;                            /* Minimum system heap size in bytes                       */
MEM_SYSTEM_HEAP_MAX_SIZE = 32K;                            /* [optional] limit system heap to size in bytes           */
REGION_ALIAS("MEM_SYSTEM_HEAP_RAM", core2_emem);           /* System heap location (e.g. communication buffers)       */

/* User Heap */
MEM_USER_HEAP_MIN_SIZE   = 20K;                            /* Minimum user heap size in bytes (used by malloc)        */
/*MEM_USER_HEAP_MAX_SIZE = 10K; */                         /* [optional] User heap maximum size to size in bytes      */
REGION_ALIAS("MEM_USER_HEAP_RAM",   core2_emem);           /* User heap memory location                               */

/* -- Map link_sections_trg.ld -------------------------------------------------------------------------------------- */
REGION_ALIAS("MEM_TRG_CODE_SC2_L1",    no_mem);            /* Safety .text located in fastest RAM area                */
REGION_ALIAS("MEM_TRG_CODE_SC2_L2",    no_mem);            /* Safety .text located in fast RAM area                   */
REGION_ALIAS("MEM_TRG_DATA_SC2_L1",    no_mem);            /* Safety data/bss manually located in DSPR                */
REGION_ALIAS("MEM_TRG_DATA_SC2_L2",    no_mem);            /* Safety data/bss manually located in EMEM                */
REGION_ALIAS("MEM_TRG_DATA_SC2_L2_UC", no_mem);            /* Safety data/bss manually located in uncached EMEM       */

REGION_ALIAS("MEM_TRG_CODE_SC0_L1",    core2_pspr);        /* Non-safe .text manually located in PSPR                 */
REGION_ALIAS("MEM_TRG_CODE_SC0_L2",    core2_emem);        /* Non-safe .text manually located in EMEM                 */
REGION_ALIAS("MEM_TRG_DATA_SC0_L1",    core2_dspr);        /* Non-safe data/bss manually located in DSPR              */
REGION_ALIAS("MEM_TRG_DATA_SC0_L2",    core2_emem);        /* Non-safe data/bss manually located in EMEM              */
REGION_ALIAS("MEM_TRG_DATA_SC0_L2_UC", core2_emem_uc);     /* Non-safe data/bss manually located in uncached EMEM     */

/* -- Support stdlib malloc/realloc/free: force linking of stdlib malloc replacements ------------------------------- */
EXTERN(malloc);
EXTERN(calloc);
EXTERN(realloc);
EXTERN(free);

/* -- Place Ethernet buffers ---------------------------------------------------------------------------------------- */
SECTIONS
{
   .ethernet_uc (NOLOAD):
   {
      /* Note we're placing ethernet net buffers here in order to avoid online emem-block and address line tests */
      KEEP(*(.ethernet_uc))
   } > core2_emem_eth_uc
}

/* -- Application start address ------------------------------------------------------------------------------------- */
MEM_START_ADDR = ORIGIN(MEM_BOOT_ROM);

/* -- Emit output sections ------------------------------------------------------------------------------------------ */
INCLUDE aurix_common_sections_begin.ld
INCLUDE signature_block.ld
INCLUDE link_sections_trg.ld
INCLUDE aurix_common_sections_end.ld
INCLUDE link_symbols_trg.ld

/*  -- Check for any unallocated input sections --------------------------------------------------------------------- */
SECTIONS
{
   .unallocated : { *(*) }

   /* Make sure that the .unallocated output section is empty  */
   _. = ASSERT (SIZEOF(.unallocated) == 0, "there are unallocated input sections");
}
