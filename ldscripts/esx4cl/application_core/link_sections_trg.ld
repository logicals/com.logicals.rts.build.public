/*!
   \file
   \brief       Target specific memory sections

   \copyright   Copyright 2019 Sensor-Technik Wiedemann GmbH. All rights reserved.

   Arguments (? = optional memory (ie. no_mem) or variable; + = Defaulted variable, only redefine if required):
      Name                      Type         Description
      MEM_BOOT_ROM              Memory       Memory into which to place the boot code
                                             (and per default the sig block)
      MEM_LOAD_ROM              Memory       Memory used for load region of .data

      MEM_TRG_CODE_SC2_L1       Memory?      Fastest memory for safety class 2 .text
      MEM_TRG_CODE_SC2_L2       Memory?      Fast memory for safety class 2 .text
      MEM_TRG_DATA_SC2_L1       Memory?      Fastest memory for safety class 2 .data/.bss
      MEM_TRG_DATA_SC2_L2       Memory?      Fast memory for safety class 2 .data/.bss
      MEM_TRG_DATA_SC2_L2_UC    Memory?      Fast memory for shared safety class 2 .data/.bss, should be region mirror
                                             of MEM_TRG_DATA_SC2_L2

      MEM_TRG_CODE_SC0_L1       Memory?      Fastest memory for safety class 0 .text
      MEM_TRG_CODE_SC0_L2       Memory?      Fast memory for safety class 0 .text
      MEM_TRG_DATA_SC0_L1       Memory?      Fastest memory for safety class 0 .data/.bss
      MEM_TRG_DATA_SC0_L2       Memory?      Fast memory for safety class 0 .data/.bss
      MEM_TRG_DATA_SC0_L2_UC    Memory?      Fast memory for shared safety class 0 .data/.bss, should be region mirror
                                             of MEM_TRG_DATA_SC0_L2
*/

SECTIONS
{
/* -- Safety Class 2 ------------------------------------------------------------------------------------------------ */

   .trg_code_sc2_L1 : ALIGN(8)
   {
      LD_TRG_MPU_CODE_SC2_L1_START = ABSOLUTE(.);
      KEEP(*(*.trg_code_sc2_L1*))
      . = ALIGN(8);
      LD_TRG_MPU_CODE_SC2_L1_END = ABSOLUTE(.);
   } > MEM_TRG_CODE_SC2_L1 AT> MEM_LOAD_ROM

   /** ---------- DATARAM L1 ---------- **/
   .trg_data_sc2_L1 : ALIGN(32)
   {
      LD_TRG_BUS_MPU_DATA_SC2_L1_START = ABSOLUTE(.);
      /* Add 8 bytes MPU safety margin if memory output section is used (If not, no padding needed.) */
      . += (ORIGIN(MEM_TRG_DATA_SC2_L1) == 0) ? 0 : 8;
      KEEP(*(*.trg_data_sc2_L1))
   } > MEM_TRG_DATA_SC2_L1 AT> MEM_LOAD_ROM

   .trg_bss_sc2_L1 (NOLOAD) : ALIGN(32)
   {
      KEEP(*(*.trg_bss_sc2_L1))
      . = ALIGN(32);
      LD_TRG_BUS_MPU_DATA_SC2_L1_END = ABSOLUTE(.);
   } AT> MEM_TRG_DATA_SC2_L1

   /* Then, a section for application defined "static" HEAP  */
    .trg_heap_sc2_L1 (NOLOAD) : ALIGN(32)
    {
      LD_TRG_HEAP_SC2_L1_START = ABSOLUTE(.);
      . += (ORIGIN(MEM_TRG_DATA_SC2_L1) == 0) ? 0 : 128;
      KEEP(*(*.trg_heap_sc2_L1))
      . = ALIGN(32);
      LD_TRG_HEAP_SC2_L1_END = ABSOLUTE(.);
    } > MEM_TRG_DATA_SC2_L1

   /** ---------- DATARAM L2 ---------- **/
   /* cached */
   .trg_data_sc2_L2 : ALIGN(32)
   {
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      LD_TRG_BUS_MPU_DATA_SC2_L2_START = ABSOLUTE(.);
      /* Add 8 bytes CPU MPU safety margin if memory output section is used (If not, no padding needed.) */
      . += ((ORIGIN(MEM_TRG_DATA_SC2_L2) == 0))  ? 0 : 8;
      KEEP(*(*.trg_data_sc2_L2))
   } > MEM_TRG_DATA_SC2_L2 AT> MEM_LOAD_ROM

   .trg_bss_sc2_L2 (NOLOAD) : ALIGN(32)
   {
      KEEP(*(*.trg_bss_sc2_L2))
      /* Use memory until cache line is complete */
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      LD_TRG_BUS_MPU_DATA_SC2_L2_END = ABSOLUTE(.);
   } AT> MEM_TRG_DATA_SC2_L2

   /* uncached (intended for publishing data to other cores or inter-operation between safety cores) */
   .trg_data_sc2_L2_uc : ALIGN(32)
   {
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      LD_TRG_BUS_MPU_DATA_SC2_L2_UC_START = ABSOLUTE(.);
      /* Add 8 bytes CPU MPU safety margin if memory output section is used (If not, no padding needed.) */
      . += ((ORIGIN(MEM_TRG_DATA_SC2_L2_UC) == 0))  ? 0 : 8;
      KEEP(*(*.trg_data_sc2_L2_uc))
   } > MEM_TRG_DATA_SC2_L2_UC AT> MEM_LOAD_ROM

   .trg_bss_sc2_L2_uc (NOLOAD) : ALIGN(32)
   {
      KEEP(*(*.trg_bss_sc2_L2_uc))
      /* Use memory until cache line is complete */
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      LD_TRG_BUS_MPU_DATA_SC2_L2_UC_END = ABSOLUTE(.);
   } AT> MEM_TRG_DATA_SC2_L2_UC

   .trg_code_sc2_L2 : ALIGN(8)
   {
      LD_TRG_MPU_CODE_SC2_L2_START = ABSOLUTE(.);
      KEEP(*(*.trg_code_sc2_L2*))
      . = ALIGN(8);
      LD_TRG_MPU_CODE_SC2_L2_END = ABSOLUTE(.);
   } > MEM_TRG_CODE_SC2_L2 AT> MEM_LOAD_ROM

   LD_TRG_MPU_CODE_SC2_L2_UC_START = LD_TRG_MPU_CODE_SC2_L2_START | 0x20000000;
   LD_TRG_MPU_CODE_SC2_L2_UC_END   = LD_TRG_MPU_CODE_SC2_L2_END   | 0x20000000;

   /* Then, a section for application defined "static" HEAP  */
   .trg_heap_sc2_L2 (NOLOAD) : ALIGN(32)
   {
      LD_TRG_HEAP_SC2_L2_START = ABSOLUTE(.);
      . += (ORIGIN(MEM_TRG_DATA_SC2_L2) == 0) ? 0 : 128;
      KEEP(*(*.trg_heap_sc2_L2))
      . = ALIGN(32);
      LD_TRG_HEAP_SC2_L2_END = ABSOLUTE(.);
   } > MEM_TRG_DATA_SC2_L2

   LD_TRG_HEAP_SC2_L2_UC_START = LD_TRG_HEAP_SC2_L2_START | 0x20000000;
   LD_TRG_HEAP_SC2_L2_UC_END   = LD_TRG_HEAP_SC2_L2_END   | 0x20000000;

/* -- Safety Class 0 ------------------------------------------------------------------------------------------------ */

   /* Fast code ram */
   .trg_code_sc0_L1 : ALIGN(8)
   {
      LD_TRG_MPU_CODE_SC0_L1_START = ABSOLUTE(.);
      KEEP(*(*.trg_code_sc0_L1*))
      . = ALIGN(8);
      LD_TRG_MPU_CODE_SC0_L1_END = ABSOLUTE(.);
   } > MEM_TRG_CODE_SC0_L1 AT> MEM_LOAD_ROM

   /* Fast .data */
   .trg_data_sc0_L1 : ALIGN(32)
   {
      LD_TRG_BUS_MPU_DATA_SC0_L1_START = ABSOLUTE(.);
      . += (ORIGIN(MEM_TRG_DATA_SC0_L1) == 0) ? 0 : 8; /* Conditional MPU margin */
      KEEP(*(*.trg_data_sc0_L1))
   } > MEM_TRG_DATA_SC0_L1 AT> MEM_LOAD_ROM

   /* Fast .bss */
   .trg_bss_sc0_L1 (NOLOAD) : ALIGN(32)
   {
      KEEP(*(*.trg_bss_sc0_L1))
      . = ALIGN(32);
      LD_TRG_BUS_MPU_DATA_SC0_L1_END = ABSOLUTE(.);
   } AT> MEM_TRG_DATA_SC0_L1

   /* Fast heap */
   .trg_heap_sc0_L1 (NOLOAD) : ALIGN(32)
   {
     LD_TRG_HEAP_SC0_L1_START = ABSOLUTE(.);
     . += (ORIGIN(MEM_TRG_DATA_SC0_L1) == 0) ? 0 : 128;
     KEEP(*(*.trg_heap_sc0_L1))
     . = ALIGN(32);
     LD_TRG_HEAP_SC0_L1_END = ABSOLUTE(.);
   } > MEM_TRG_DATA_SC0_L1

   /* Medium .data cached */
   .trg_data_sc0_L2 : ALIGN(32)
   {
      LD_TRG_BUS_MPU_DATA_SC0_L2_START = ABSOLUTE(.);
      . += ((ORIGIN(MEM_TRG_DATA_SC0_L2) | ORIGIN(MEM_TRG_DATA_SC0_L2_UC) == 0)) ? 0 : 8; /* Conditional MPU margin */
      KEEP(*(*.trg_data_sc0_L2))
   } > MEM_TRG_DATA_SC0_L2 AT> MEM_LOAD_ROM

   /* Medium .bss cached */
   .trg_bss_sc0_L2 (NOLOAD) : ALIGN(32)
   {
      KEEP(*(*.trg_bss_sc0_L2))
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      LD_TRG_BUS_MPU_DATA_SC0_L2_END = ABSOLUTE(.);
   } AT> MEM_TRG_DATA_SC0_L2

   /* Medium code ram */
   .trg_code_sc0_L2 : ALIGN(8)
   {
      LD_TRG_MPU_CODE_SC0_L2_START = ABSOLUTE(.);
      KEEP(*(*.trg_code_sc0_L2*))
      . = ALIGN(8);
      LD_TRG_MPU_CODE_SC0_L2_END = ABSOLUTE(.);
   } > MEM_TRG_CODE_SC0_L2 AT> MEM_LOAD_ROM

   LD_TRG_MPU_CODE_SC0_L2_UC_START = LD_TRG_MPU_CODE_SC0_L2_START | 0x20000000;
   LD_TRG_MPU_CODE_SC0_L2_UC_END   = LD_TRG_MPU_CODE_SC0_L2_END   | 0x20000000;

   /* Medium heap cached  */
    .trg_heap_sc0_L2 (NOLOAD) : ALIGN(32)
    {
      LD_TRG_HEAP_SC0_L2_START = ABSOLUTE(.);
      . += ((ORIGIN(MEM_TRG_DATA_SC0_L2) == 0) || (ORIGIN(MEM_TRG_DATA_SC2_L2) != 0)) ? 0 : 128;
      KEEP(*(*.trg_heap_sc0_L2))
      . = ALIGN(32);
      LD_TRG_HEAP_SC0_L2_END = ABSOLUTE(.);
    } > MEM_TRG_DATA_SC0_L2

   LD_TRG_HEAP_SC0_L2_UC_START = LD_TRG_HEAP_SC0_L2_START | 0x20000000;
   LD_TRG_HEAP_SC0_L2_UC_END   = LD_TRG_HEAP_SC0_L2_END   | 0x20000000;

   /* Medium .data uncached */
   .trg_data_sc0_L2_uc : ALIGN(32)
   {
      LD_TRG_BUS_MPU_DATA_SC0_L2_UC_START = ABSOLUTE(.);
      . += (ORIGIN(MEM_TRG_DATA_SC0_L2_UC) == 0)  ? 0 : 8; /* Conditional MPU margin */
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      KEEP(*(*.trg_data_sc0_L2_uc))
   } > MEM_TRG_DATA_SC0_L2_UC AT> MEM_LOAD_ROM

   /* Medium .bss uncached */
   .trg_bss_sc0_L2_uc (NOLOAD) : ALIGN(32)
   {
      KEEP(*(*.trg_bss_sc0_L2_uc))
      . = ALIGN(CPU_CACHE_LINE_SIZE);
      LD_TRG_BUS_MPU_DATA_SC0_L2_UC_END = ABSOLUTE(.);
   } AT> MEM_TRG_DATA_SC0_L2_UC

/* -- Copy/Clear sections ------------------------------------------------------------------------------------------- */

   .clear_sec : ALIGN(4)
   {
      LONG(0 + ADDR(.trg_bss_sc2_L1));    LONG(SIZEOF(.trg_bss_sc2_L1));
      LONG(0 + ADDR(.trg_bss_sc0_L1));    LONG(SIZEOF(.trg_bss_sc0_L1));

      LONG(0 + ADDR(.trg_bss_sc2_L2));    LONG(SIZEOF(.trg_bss_sc2_L2));
      LONG(0 + ADDR(.trg_bss_sc2_L2_uc)); LONG(SIZEOF(.trg_bss_sc2_L2_uc));
      LONG(0 + ADDR(.trg_bss_sc0_L2));    LONG(SIZEOF(.trg_bss_sc0_L2));
      LONG(0 + ADDR(.trg_bss_sc0_L2_uc)); LONG(SIZEOF(.trg_bss_sc0_L2_uc));
   } AT> MEM_BOOT_ROM

   .copy_sec : ALIGN(4)
   {
      LONG(LOADADDR(.trg_data_sc2_L1));    LONG(0 + ADDR(.trg_data_sc2_L1));      LONG(SIZEOF(.trg_data_sc2_L1));
      LONG(LOADADDR(.trg_data_sc0_L1));    LONG(0 + ADDR(.trg_data_sc0_L1));      LONG(SIZEOF(.trg_data_sc0_L1));

      LONG(LOADADDR(.trg_data_sc2_L2));    LONG(0 + ADDR(.trg_data_sc2_L2));      LONG(SIZEOF(.trg_data_sc2_L2));
      LONG(LOADADDR(.trg_data_sc2_L2_uc)); LONG(0 + ADDR(.trg_data_sc2_L2_uc));   LONG(SIZEOF(.trg_data_sc2_L2_uc));

      LONG(LOADADDR(.trg_data_sc0_L2));    LONG(0 + ADDR(.trg_data_sc0_L2));      LONG(SIZEOF(.trg_data_sc0_L2));
      LONG(LOADADDR(.trg_data_sc0_L2_uc)); LONG(0 + ADDR(.trg_data_sc0_L2_uc));   LONG(SIZEOF(.trg_data_sc0_L2_uc));

      LONG(LOADADDR(.trg_code_sc2_L1));    LONG(0 + ADDR(.trg_code_sc2_L1));      LONG(SIZEOF(.trg_code_sc2_L1));
      LONG(LOADADDR(.trg_code_sc0_L1));    LONG(0 + ADDR(.trg_code_sc0_L1));      LONG(SIZEOF(.trg_code_sc0_L1));
      LONG(LOADADDR(.trg_code_sc2_L2));    LONG(0 + ADDR(.trg_code_sc2_L2));      LONG(SIZEOF(.trg_code_sc2_L2));
   } AT> MEM_BOOT_ROM
}
