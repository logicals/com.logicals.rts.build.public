/*!
   \file
   \brief       DLMU memory default P4 sections and symbols

   Conditional Memories (alias to "no_mem" if not required):

      MEM_DLMU_DATA_SC2:    Safety data
      MEM_DLMU_DATA_SC2_UC: Safety data uncached

      MEM_DLMU_DATA_SC0:    Nonsafe data
      MEM_DLMU_DATA_SC0_UC: Nonsafe data uncached

      MEM_DLMU_CODE:        Code sections
      MEM_DLMU_STACKS:      Stack sections

   \copyright   Copyright 2022 Sensor-Technik Wiedemann GmbH. All rights reserved.
*/

SECTIONS {

   _. = ASSERT ((LENGTH(MEM_DLMU_DATA_SC0) == 0 || LENGTH(MEM_DLMU_DATA_SC0_UC) != 0), "You must map both cached and uncached dlmu");
   _. = ASSERT ((LENGTH(MEM_DLMU_DATA_SC2) == 0 || LENGTH(MEM_DLMU_DATA_SC2_UC) != 0), "You must map both cached and uncached dlmu");

   /* -- SC0/SC2 Code ----------------------------------------------------------------------------------------------- */

   .dlmu.code : ALIGN(32)
   {
      LD_32_DLMU_CODE_START = ABSOLUTE(.);
      LD_32_DLMU_CODE_SC2_START = ABSOLUTE(.);
      KEEP(*(*.dlmu.code.sc2))
      KEEP(*(*.dlmu.code.sc2.*))
      . = ALIGN(32);
      LD_32_DLMU_CODE_SC2_END = ABSOLUTE(.);
      LD_32_DLMU_CODE_SC0_START = ABSOLUTE(.);
      KEEP(*(*.dlmu.code.sc0))
      KEEP(*(*.dlmu.code.sc0.*))
      . = ALIGN(32);
      LD_32_DLMU_CODE_SC0_END = ABSOLUTE(.);
      LD_32_DLMU_CODE_END = ABSOLUTE(.);
   } > MEM_DLMU_CODE AT> MEM_LOAD_ROM

   /* -- Stack regions ---------------------------------------------------------------------------------------------- */

   .dlmu.stacks (NOLOAD) : ALIGN(32)
   {
      LD_32_DLMU_STACKS_START = ABSOLUTE(.);
      . += (LENGTH(MEM_DLMU_STACKS) != 0) ? 128 : 0;  /* guard zone */
      LD_32_DLMU_STACKS_SC2_START = ABSOLUTE(.);
      KEEP(*(*.dlmu.stacks.sc2))
      . = ALIGN(32);
      LD_32_DLMU_STACKS_SC2_END = ABSOLUTE(.);
      LD_32_DLMU_STACKS_SC0_START = ABSOLUTE(.);
      KEEP(*(*.dlmu.stacks.sc0))
      . = ALIGN(32);
      LD_32_DLMU_STACKS_SC0_END = ABSOLUTE(.);
      . += (LENGTH(MEM_DLMU_STACKS) != 0) ? 128 : 0;  /* guard zone */
      LD_32_DLMU_STACKS_END     = ABSOLUTE(.);
   } > MEM_DLMU_STACKS

   LD_32_DLMU_STACKS_UC_START = LD_32_DLMU_STACKS_START | 0x20000000;
   LD_32_DLMU_STACKS_UC_END   = LD_32_DLMU_STACKS_END   | 0x20000000;

   /* -- SC2 -------------------------------------------------------------------------------------------------------- */

   .dlmu.data.sc2.c : ALIGN(32)
   {
      LD_32_DLMU_DATA_SC2_START = ABSOLUTE(.);
      . += (LENGTH(MEM_DLMU_DATA_SC2) == 0) ? 0 : 8; /* Conditional MPU margin */
      KEEP(*(*.dlmu.data.sc2.c))
      KEEP(*(*.dlmu.data.sc2.c.*))
   } > MEM_DLMU_DATA_SC2 AT> MEM_LOAD_ROM

   .dlmu.bss.sc2.c (NOLOAD) : ALIGN(4)
   {
      KEEP(*(*.dlmu.bss.sc2.c))
      KEEP(*(*.dlmu.bss.sc2.c.*))
   } > MEM_DLMU_DATA_SC2

   .dlmu.data.sc2.uc : ALIGN(32)
   {
      KEEP(*(*.dlmu.data.sc2.uc))
      KEEP(*(*.dlmu.data.sc2.uc.*))
   } > MEM_DLMU_DATA_SC2_UC AT> MEM_LOAD_ROM

   .dlmu.bss.sc2.uc : ALIGN(4)
   {
      KEEP(*(*.dlmu.bss.sc2.uc))
      KEEP(*(*.dlmu.bss.sc2.uc.*))
      . = ALIGN(32);
      LD_32_DLMU_DATA_SC2_UC_END = ABSOLUTE(.);
   } > MEM_DLMU_DATA_SC2_UC

   LD_32_DLMU_DATA_SC2_UC_START = LD_32_DLMU_DATA_SC2_START  |   0x20000000;
   LD_32_DLMU_DATA_SC2_END      = LD_32_DLMU_DATA_SC2_UC_END & ~ 0x20000000;

   /* -- SC0 -------------------------------------------------------------------------------------------------------- */

   .dlmu.data.sc0.c : ALIGN(32)
   {
      LD_32_DLMU_DATA_SC0_START = ABSOLUTE(.);
      KEEP(*(*.dlmu.data.sc0.c))
      KEEP(*(*.dlmu.data.sc0.c.*))
   } > MEM_DLMU_DATA_SC0 AT> MEM_LOAD_ROM

   .dlmu.bss.sc0.c (NOLOAD) : ALIGN(4)
   {
      KEEP(*(*.dlmu.bss.sc0.c))
      KEEP(*(*.dlmu.bss.sc0.c.*))
      . = ALIGN(32);
   } > MEM_DLMU_DATA_SC0

   .dlmu.data.sc0.uc : ALIGN(32)
   {
      KEEP(*(*.dlmu.data.sc0.uc))
      KEEP(*(*.dlmu.data.sc0.uc.*))
   } > MEM_DLMU_DATA_SC0_UC AT> MEM_LOAD_ROM

   .dlmu.bss.sc0.uc : ALIGN(4)
   {
      KEEP(*(*.dlmu.bss.sc0.uc))
      KEEP(*(*.dlmu.bss.sc0.uc.*))
      . = ALIGN(32);
      LD_32_DLMU_DATA_SC0_UC_END = ABSOLUTE(.);
   } > MEM_DLMU_DATA_SC0_UC

   LD_32_DLMU_DATA_SC0_UC_START = LD_32_DLMU_DATA_SC0_START  |   0x20000000;
   LD_32_DLMU_DATA_SC0_END      = LD_32_DLMU_DATA_SC0_UC_END & ~ 0x20000000;

   /* -- Combined data block ---------------------------------------------------------------------------------------- */

   LD_32_DLMU_DATA_START        = LD_32_DLMU_DATA_SC2_START;
   LD_32_DLMU_DATA_END          = LD_32_DLMU_DATA_SC0_END;
   LD_32_DLMU_DATA_UC_START     = LD_32_DLMU_DATA_SC2_UC_START;
   LD_32_DLMU_DATA_UC_END       = LD_32_DLMU_DATA_SC0_UC_END;

   /* -- Copy/Clear sections ---------------------------------------------------------------------------------------- */

   .clear_sec : ALIGN(4)
   {
      LONG(0 + ADDR(.dlmu.bss.sc0.c));  LONG(SIZEOF(.dlmu.bss.sc0.c));
      LONG(0 + ADDR(.dlmu.bss.sc2.c));  LONG(SIZEOF(.dlmu.bss.sc2.c));
      LONG(0 + ADDR(.dlmu.bss.sc0.uc)); LONG(SIZEOF(.dlmu.bss.sc0.uc));
      LONG(0 + ADDR(.dlmu.bss.sc2.uc)); LONG(SIZEOF(.dlmu.bss.sc2.uc));
   } AT> MEM_BOOT_ROM

   .copy_sec : ALIGN(4)
   {
      LONG(LOADADDR(.dlmu.data.sc2.c));  LONG(0 + ADDR(.dlmu.data.sc2.c));  LONG(SIZEOF(.dlmu.data.sc2.c));
      LONG(LOADADDR(.dlmu.data.sc0.c));  LONG(0 + ADDR(.dlmu.data.sc0.c));  LONG(SIZEOF(.dlmu.data.sc0.c));
      LONG(LOADADDR(.dlmu.data.sc2.uc)); LONG(0 + ADDR(.dlmu.data.sc2.uc)); LONG(SIZEOF(.dlmu.data.sc2.uc));
      LONG(LOADADDR(.dlmu.data.sc0.uc)); LONG(0 + ADDR(.dlmu.data.sc0.uc)); LONG(SIZEOF(.dlmu.data.sc0.uc));
      LONG(LOADADDR(.dlmu.code));        LONG(0 + ADDR(.dlmu.code));        LONG(SIZEOF(.dlmu.code));
   } AT> MEM_BOOT_ROM

}
