/*!
   \file
   \brief       Linker script for C applications

   \copyright   Copyright 2019 Sensor-Technik Wiedemann GmbH. All rights reserved.
*/

/* Include platform memories and core interop sections */
INCLUDE tc367_osy_memdef.ld

/* -- Memory partitioning (flash, lmu ram, emem) -------------------------------------------------------------------- */
MEMORY_LAYOUT_IDENTIFIER = 1;                 /* The memory layout identifier must match across all core applications */

MEMORY
{
/* Flash Partitioning */
   core0_pflash_seg0     (rx) :   org = ORIGIN(available_pflash0),       len = LENGTH(available_pflash0) - 1M
   core0_pflash_seg1     (rx) :   org = ORIGIN(available_pflash0) + LENGTH(available_pflash0) - 1M,  len = 1M

   core1_pflash_seg0     (rx) :   org = ORIGIN(available_pflash1),       len = LENGTH(available_pflash1) - 1M
   core1_pflash_seg1     (rx) :   org = ORIGIN(available_pflash1) + 1M,  len = LENGTH(available_pflash1) - 1M

   dlmu                 (w!x) :   org = ORIGIN(core0_dlmu),              len = LENGTH(core0_dlmu) + LENGTH(core1_dlmu)

   core0_dspr_trg       (w!x) :   org = ORIGIN(core1_dspr) + 192K - 40K, len = 40K - 64 /* core1 dspr usable by core 0*/

   var_flash             (rx) :   org = ORIGIN(dflash1),                 len = 4K  /* Data Flash          (4 KiB) */
}

/* Define uncached counterparts where applicable */
MEMORY
{
   core0_pflash_seg0_uc  (rx) :   org = ORIGIN(core0_pflash_seg0) | 0x20000000, len = LENGTH(core0_pflash_seg0)
   core0_pflash_seg1_uc  (rx) :   org = ORIGIN(core0_pflash_seg1) | 0x20000000, len = LENGTH(core0_pflash_seg1)
   core1_pflash_seg0_uc  (rx) :   org = ORIGIN(core1_pflash_seg0) | 0x20000000, len = LENGTH(core1_pflash_seg0)
   core1_pflash_seg1_uc  (rx) :   org = ORIGIN(core1_pflash_seg1) | 0x20000000, len = LENGTH(core1_pflash_seg1)
   dlmu_uc               (rx) :   org = ORIGIN(dlmu) | 0x20000000,              len = LENGTH(dlmu)
}

REGION_MIRROR("core0_pflash_seg0_uc", "core0_pflash_seg0")
REGION_MIRROR("core0_pflash_seg1_uc", "core0_pflash_seg1")
REGION_MIRROR("core1_pflash_seg0_uc", "core1_pflash_seg0")
REGION_MIRROR("core1_pflash_seg1_uc", "core1_pflash_seg1")

/* Define aliases for "our" memories to be referenced later */
REGION_ALIAS("MEM_MY_DSPR",           core0_dspr);
REGION_ALIAS("MEM_MY_PSPR",           core0_pspr);
REGION_ALIAS("MEM_MY_DLMU",           dlmu);
REGION_ALIAS("MEM_MY_DLMU_UC",        dlmu_uc);
REGION_ALIAS("MEM_MY_LMU",            no_mem);
REGION_ALIAS("MEM_MY_LMU_UC",         no_mem);
REGION_ALIAS("MEM_MY_DAM",            no_mem);
REGION_ALIAS("MEM_MY_DAM_UC",         no_mem);
REGION_ALIAS("MEM_MY_EMEM",           no_mem);
REGION_ALIAS("MEM_MY_EMEM_UC",        no_mem);
REGION_ALIAS("MEM_MY_BOOT_ROM",       core0_boot_rom);
REGION_ALIAS("MEM_MY_PFLASH_SEG0",    core0_pflash_seg0);
REGION_ALIAS("MEM_MY_PFLASH_SEG0_UC", core0_pflash_seg0_uc);
REGION_ALIAS("MEM_MY_PFLASH_SEG1",    core0_pflash_seg1);
REGION_ALIAS("MEM_MY_PFLASH_SEG1_UC", core0_pflash_seg1_uc);
REGION_ALIAS("MEM_MY_PFLASH_SEG2",    no_mem);
REGION_ALIAS("MEM_MY_PFLASH_SEG2_UC", no_mem);


/* -- Map aurix_common_sections_begin.ld and aurix_common_sections_end.ld ------------------------------------------- */
MEM_ISTACK_SIZE  =  4K;                                    /* Interrupt stack size in bytes                           */
MEM_USTACK_SIZE  = 20K;                                    /* User stack size in bytes                                */
MEM_CSA_MIN_SIZE = 24K;                                    /* Minimum size of CSA area in bytes                       */
MEM_LCX_HEADROOM =   6;                                    /* Number of reserved lower contexts for trap handling     */
MEM_USE_SAFETLIB =   0;                                    /* Use SafeTlib (use defaults for dpsr and pspr sizes)     */

REGION_ALIAS("MEM_BOOT_ROM",       MEM_MY_BOOT_ROM);       /* Startup code and data placed at static address          */
REGION_ALIAS("MEM_LOAD_ROM",       MEM_MY_PFLASH_SEG0);    /* Data/Code load address area                             */

REGION_ALIAS("MEM_PSPR",           MEM_MY_PSPR);           /* PSPR coupled with this core                             */
REGION_ALIAS("MEM_DSPR",           MEM_MY_DSPR);           /* DSPR coupled with this core                             */
REGION_ALIAS("MEM_LMU",            no_mem);                /* not available                                           */
REGION_ALIAS("MEM_LMU_UC",         no_mem);                /* not available                                           */
REGION_ALIAS("MEM_HANDLES",        MEM_MY_DSPR);           /* Memory region for system object handles                 */

REGION_ALIAS("MEM_SDATA_BIOS_RAM", MEM_MY_DSPR);           /* Small data for internal usage                           */
REGION_ALIAS("MEM_SDATA_BIOS_ROM", MEM_MY_PFLASH_SEG0);    /* Small rom for internal usage                            */
REGION_ALIAS("MEM_SDATA_USER_RAM", MEM_MY_DLMU);           /* Small ram data reserved for user                        */
REGION_ALIAS("MEM_SDATA_USER_ROM", MEM_MY_PFLASH_SEG0);    /* Small rom reserved for user                             */

REGION_ALIAS("MEM_DEFAULT_ROM_0",  MEM_MY_PFLASH_SEG0);    /* Application default .text and .rodata section           */
REGION_ALIAS("MEM_DEFAULT_ROM_1",  MEM_MY_PFLASH_SEG1);    /* Application default .text and .rodata (continued)       */
REGION_ALIAS("MEM_DEFAULT_ROM_2",  MEM_MY_PFLASH_SEG2);    /* Application default .text and .rodata (continued)       */
REGION_ALIAS("MEM_DEFAULT_ROM_3",  no_mem);                /* Application default .text and .rodata (continued)       */
REGION_ALIAS("MEM_DEFAULT_ROM_4",  no_mem);

REGION_ALIAS("MEM_DEFAULT_RAM_0",  dlmu);                  /* Application default .data and .bss section              */
REGION_ALIAS("MEM_DEFAULT_RAM_1",  core0_dspr_trg);        /* Application default .data and .bss section (continued)  */
REGION_ALIAS("MEM_DEFAULT_RAM_2",  MEM_MY_DSPR);           /* Application default .data and .bss section (continued)  */
REGION_ALIAS("MEM_DEFAULT_RAM_3",  no_mem);                /* Application default .data and .bss section (continued)  */
REGION_ALIAS("MEM_DEFAULT_RAM_4",  no_mem);                /* Application default .data and .bss section (continued)  */

/* System Heap */
MEM_SYSTEM_HEAP_MIN_SIZE = 10K;                            /* Minimum system heap size in bytes                       */
MEM_SYSTEM_HEAP_MAX_SIZE = 32K;                            /* [optional] limit system heap to size in bytes           */
REGION_ALIAS("MEM_SYSTEM_HEAP_RAM", MEM_MY_DSPR);          /* System heap location (e.g. communication buffers)       */

/* User Heap */
MEM_USER_HEAP_MIN_SIZE   = 20K;                            /* Minimum user heap size in bytes (used by malloc)        */
MEM_USER_HEAP_MAX_SIZE   = 32K;                            /* [optional] User heap maximum size to size in bytes      */
REGION_ALIAS("MEM_USER_HEAP_RAM",   MEM_MY_DLMU);          /* User heap memory location                               */

/* -- Target specific optional sections and memories ---------------------------------------------------------------- */

/* xll_memtypes_trg.ld */
REGION_ALIAS("MEM_TRG_ICC_RESOURCES",  MEM_MY_DSPR);

/* xll_memtype_dspr.ld */
REGION_ALIAS("MEM_DSPR_STACKS",        MEM_MY_DSPR);
REGION_ALIAS("MEM_DSPR_DATA_SC2",      MEM_MY_DSPR);
REGION_ALIAS("MEM_DSPR_DATA_SC0",      MEM_MY_DSPR);
REGION_ALIAS("MEM_DSPR_CODE",          MEM_MY_DSPR);

/* xll_memtype_dlmu.ld */
REGION_ALIAS("MEM_DLMU_STACKS",        MEM_MY_DLMU);
REGION_ALIAS("MEM_DLMU_DATA_SC2",      MEM_MY_DLMU);
REGION_ALIAS("MEM_DLMU_DATA_SC2_UC",   MEM_MY_DLMU_UC);
REGION_ALIAS("MEM_DLMU_DATA_SC0",      MEM_MY_DLMU);
REGION_ALIAS("MEM_DLMU_DATA_SC0_UC",   MEM_MY_DLMU_UC);
REGION_ALIAS("MEM_DLMU_CODE",          MEM_MY_DLMU);

/* -- Support stdlib malloc/realloc/free: force linking of stdlib malloc replacements ------------------------------- */
EXTERN(malloc);
EXTERN(calloc);
EXTERN(realloc);
EXTERN(free);

/* -- Place ABHMD --------------------------------------------------------------------------------------------------- */
SECTIONS
{
   .BMI_Header :
   {
      KEEP(*(.BMI_Header))
   } > MEM_BOOT_ROM = 0
}
_. = ASSERT (SIZEOF(.BMI_Header) == 0x20, "BMI Header must be defined for this target build!");

/* -- Application start address ------------------------------------------------------------------------------------- */
MEM_START_ADDR = ORIGIN(MEM_BOOT_ROM) + 0x20;

/* -- Emit output sections ------------------------------------------------------------------------------------------ */
INCLUDE aurix_common_sections_begin.ld
INCLUDE signature_block.ld
INCLUDE xll_memtypes_trg.ld
INCLUDE xll_memtype_dspr.ld
INCLUDE xll_memtype_dlmu.ld
INCLUDE aurix_common_sections_end.ld

/*  -- Check for any unallocated input sections --------------------------------------------------------------------- */
SECTIONS
{
   .unallocated : { *(*) }

   /* Make sure that the .unallocated output section is empty  */
   _. = ASSERT (SIZEOF(.unallocated) == 0, "there are unallocated input sections");
}

/*  -- Set Variant Structure ---------------------------------------------------------------------------------------- */
gt_XbVar4ct = ORIGIN(var_flash);
