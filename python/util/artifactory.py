import requests
from buildutil import calc_md5_from_file, calc_sha1_from_file


class Artifactory(object):

    ARTIFACTORY_URL = 'https://artifactory.logicals.com/artifactory'

    def __init__(self, user, password):
        self.user = user
        self.password = password

    def upload(self, path):
        pass

    def download(self,
                 repositoryPrefix, groupId, artifactId,
                 version, classifier, fileType,
                 destination):
        with open(destination, 'wb') as handle:
            path = self.__build_repo_path(repositoryPrefix, groupId,
                                          artifactId, version, classifier,
                                          fileType)
            response = requests.get(
                    path,
                    auth=requests.auth.HTTPBasicAuth(self.user, self.password),
                    stream=True)
            if not response.ok:
                raise Exception("Could not download artifact: " + path)
            for block in response.iter_content(1024):
                handle.write(block)

    def upload(self,
               repositoryPrefix, groupId, artifactId,
               version, classifier, fileType,
               source):
        with open(source, 'rb') as handle:
            path = self.__build_repo_path(repositoryPrefix, groupId,
                                          artifactId, version, classifier,
                                          fileType)
            md5 = calc_md5_from_file(source)
            sha1 = calc_sha1_from_file(source)
            http_auth = requests.auth.HTTPBasicAuth(self.user, self.password)
            response = requests.delete(path, auth=http_auth)
            if not response.ok:
                print("Artifact could not be found yet: " + path)
            response = requests.put(
                    path,
                    auth=http_auth,
                    data=handle,
                    headers={ 'X-Checksum-Md5': md5,
                              'X-Checksum-Sha1': sha1 })
            if not response.ok:
                response.raise_for_status()
            print(response.text)

    def __build_repo_path(self,
                          repositoryPrefix, groupId, artifactId,
                          version, classifier, fileType):
        if version.find('SNAPSHOT') < 0:
            repository = repositoryPrefix + '-release-local'
        else:
            repository = repositoryPrefix + '-snapshot-local'

        classifierStr = '-' + classifier if classifier != '' else ''

        return Artifactory.ARTIFACTORY_URL + '/' + repository + '/' + \
            groupId.replace('.', '/') + '/' + \
            artifactId + '/' + version + '/' + artifactId + '-' + \
            version + classifierStr + '.' + fileType
