import json

from buildutil import execute_process
from pathutil import NormalizedPath


class PClint(object):
    def parse(self, pathToCfg):
        with open(pathToCfg, "r") as fp:
            cfg = json.load(fp)
            pclint_cmd = []
            pclint_cmd += [cfg["LintExecutable"]]
            if len(cfg["ConfigParams"]) > 0:
                pclint_cmd += cfg["ConfigParams"].split(';')
            if len(cfg["UserFlags"]) > 0:
                pclint_cmd += ['-u']
                pclint_cmd += cfg["UserFlags"].split(';')
            pclint_cmd.append(cfg["ExtraFlags"])
            if len(cfg["IncludeDirs"]) > 0:
                pclint_cmd += [
                    "-i" + incDir for incDir in cfg["IncludeDirs"].split(';')
                ]
            if len(cfg["Defines"]) > 0:
                pclint_cmd += [
                    "-d" + incDir for incDir in cfg["Defines"].split(';')
                ]
            pclint_cmd += cfg["SourceFiles"].split(';')
            execute_process("PC-lint", pclint_cmd)

    def parseDir(self, dirPath):
        for dPath in NormalizedPath(dirPath).glob('*_lint.json'):
            self.parse(dPath.toHostPath())
