import platform

__TARGET_PLATFORM_RUNNABLE_ON = {
    'linux-x64': ['Linux-x86_64'],
    'linux-x86': ['Linux-x86_64', 'Linux-x86'],
    'nt-x86': ['Windows-AMD64'],
    'nt-x64': ['Windows-AMD64'],
    'darwin-x64': ['Darwin-x86_64']
}


def host_platform_is_compatible_with(target_platform):
    if not target_platform in __TARGET_PLATFORM_RUNNABLE_ON:
        return False
    return (platform.system() + "-" + platform.machine()) in \
        __TARGET_PLATFORM_RUNNABLE_ON[target_platform]
