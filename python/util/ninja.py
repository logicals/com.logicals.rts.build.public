from buildutil import execute_process

class Ninja(object):

    NINJA_COMMAND = "ninja"

    def build(self, target="", verbose=True, runSerial=False):
        ninja_command = [Ninja.NINJA_COMMAND]
        if verbose:
            ninja_command += ["-v"]
        if runSerial:
            ninja_command += ["-j 1"]
        if target != "":
            ninja_command += [target]
        execute_process("Ninja", ninja_command)
