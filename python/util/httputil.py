import requests

def http_download(path, destination, user=None, password=None):
    with open(destination, 'wb') as handle:
        httpAuth = None
        if user is not None and password is not None:
            httpAuth = requests.auth.HTTPBasicAuth(user, password)
        response = requests.get(
                path,
                auth=httpAuth,
                stream=True)
        if not response.ok:
            raise Exception("Could not download file: " + path)
        for block in response.iter_content(1024):
            handle.write(block)



