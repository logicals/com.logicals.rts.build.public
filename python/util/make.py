from buildutil import execute_process


class Make(object):
    COMMAND = "make"

    def execute(self, make_vars={}, makefile='Makefile'):
        make_command = [ Make.COMMAND, "-j", "8" ]
        for key, value in make_vars.items():
            make_command += [ key + '=' + value ]
        make_command += [ '-f', makefile ]
        execute_process("Make", make_command)



