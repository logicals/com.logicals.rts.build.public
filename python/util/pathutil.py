#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path
import os


class NormalizedPath(object):
    def __init__(self, *segments):
        self.__path = Path(*segments).resolve()

    def toRawPath(self):
        retVal = self.toHostPath()
        return retVal.replace(os.sep, '/')

    def toHostPath(self):
        return str(self.__path)

    def getParentFolder(self, depth=0):
        return NormalizedPath(self.__path.parents[depth])

    def glob(self, globExpr):
        return [NormalizedPath(str(path))
                for path in self.__path.glob(globExpr)]
