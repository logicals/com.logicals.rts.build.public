#!/usr/bin/env python
# -*- coding: utf-8 -*-

from buildutil import execute_process, is_tool_available, create_and_clean_dir
from pathutil import NormalizedPath


class CoverageToolGcovr(object):
    XML = 1
    HTML = 2

    def __init__(self, binaryDir, sourceDir, filters=[]):
        self.binaryDir = binaryDir
        self.sourceDir = sourceDir
        self.filters = filters
        self.__toolsAvailable = is_tool_available(
            'gcov') and is_tool_available('gcovr')

    def createReport(self, outputFormat=XML):
        if not self.__toolsAvailable:
            print(
                "gcovr not available on system. "
                "Not performing code coverage analysis."
            )
            return
        if outputFormat is CoverageToolGcovr.XML:
            command = [
                'gcovr', '--xml', '-r', self.sourceDir,
                '--object-directory=' + self.binaryDir
            ]
            for f in self.filters:
                command += ['-f',
                            NormalizedPath(self.sourceDir, f).toRawPath()
                            ]
            command += ['-o',
                        NormalizedPath(self.binaryDir,
                                       'coverage_report.xml').toHostPath()]
            execute_process('gcovr', command)
        elif outputFormat is CoverageToolGcovr.HTML:
            outputDir = NormalizedPath(
                self.binaryDir, 'coverage_report').toHostPath()
            create_and_clean_dir(outputDir)
            command = [
                'gcovr', '--html', '--html-details', '-r', self.sourceDir,
                '--object-directory=' + self.binaryDir
            ]
            for f in self.filters:
                command += ['-f',
                            NormalizedPath(self.sourceDir, f).toRawPath()
                            ]
            command += ['-o',
                        NormalizedPath(outputDir, 'index.html').toHostPath()]
            execute_process('gcovr', command)
        else:
            pass


class CoverageToolLcov(object):
    def __init__(self):
        self.__toolsAvailable = is_tool_available(
            'gcov') and is_tool_available('lcov') and is_tool_available(
                'genhtml')

    def createReport(self, binaryDir, sourceDir):
        if not self.__toolsAvailable:
            return
        execute_process("lcov", [
            'lcov', '--gcov-tool', 'gcov', '--directory', '.', '--capture',
            '--output-file', 'coverage_report.info'
        ])
        execute_process(
            'genhtml',
            ['genhtml', '-o', 'coverage_report', 'coverage_report.info'])
