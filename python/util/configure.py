from buildutil import execute_process


class Configure(object):
    COMMAND = "./configure"

    def execute(self, options, configure_vars={}):
        configure_command = [ Configure.COMMAND ]
        configure_command += options
        for key, value in configure_vars.items():
            configure_command += [ key + '=' + value ]
        execute_process("Configure", configure_command)


