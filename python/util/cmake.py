import os
import shutil
from buildutil import execute_process

class CMake(object):
    CMAKE_COMMAND = "cmake"

    def generate(self, srcdir, generator, toolchain, defines):
        cmake_command = [ CMake.CMAKE_COMMAND, "-G" + generator, "-DCMAKE_TOOLCHAIN_FILE=" + toolchain ]
        cmake_command += [ "-D" + key + "=" + value for key, value in defines.items() ]
        cmake_command += [ srcdir ]
        execute_process("CMake", cmake_command)
    def delete_cache(self):
        if os.path.isdir(os.path.join(os.getcwd(),'CMakeFiles')):
            shutil.rmtree(os.path.join(os.getcwd(), 'CMakeFiles'))
        if os.path.isfile(os.path.join(os.getcwd(), 'CMakeCache.txt')):
            os.remove(os.path.join(os.getcwd(), 'CMakeCache.txt'))
