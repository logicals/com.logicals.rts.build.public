from buildutil import execute_process


class AutoReconf(object):
    COMMAND = "autoreconf"

    def execute(self, options=[]):
        autoreconf_command = [ AutoReconf.COMMAND ]
        autoreconf_command += options
        execute_process("AutoReconf", autoreconf_command)

