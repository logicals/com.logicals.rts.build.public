from buildutil import execute_process

import os
import csv
from multiprocessing import cpu_count
from math import floor


class CTest(object):
    CTEST_COMMAND = "ctest"
    CTC_INTEGRATION_TEST = "integrationTest"
    CTC_UNIT_TEST = "unitTest"

    def __init__(self, binaryDir=None, testPlanFile=None, sourceDir=None):
        self.binaryDir = binaryDir
        self.sourceDir = sourceDir
        self.testPlan = []
        if testPlanFile is not None:
            with open(testPlanFile, 'r') as tpf:
                self.testPlan = [tuple(line) for line in csv.reader(tpf)]

    def runSerial(self,
                  timeout=30,
                  ctcTestRegexIncludeStr=None,
                  ctcTestRegexExcludeStr=None):
        self.__run(timeout=timeout,
                   jobs=1,
                   ctcTestRegexIncludeStr=ctcTestRegexIncludeStr,
                   ctcTestRegexExcludeStr=ctcTestRegexExcludeStr)

    def runParallel(self,
                    timeout=30,
                    jobs=floor(cpu_count() * .75),
                    ctcTestRegexIncludeStr=None,
                    ctcTestRegexExcludeStr=None):
        print(
            "Warning: Coverage results could be wrong when running jobs in parallel."
        )
        self.__run(timeout=timeout,
                   jobs=jobs,
                   ctcTestRegexIncludeStr=ctcTestRegexIncludeStr,
                   ctcTestRegexExcludeStr=ctcTestRegexExcludeStr)

    def __run(self,
              timeout,
              jobs,
              ctcTestRegexIncludeStr=None,
              ctcTestRegexExcludeStr=None):
        for testData in self.testPlan:
            logFilePath = os.path.join(self.binaryDir, testData[1])
            try:
                with open(logFilePath, 'w') as logFile:
                    ctest_command = [
                        CTest.CTEST_COMMAND, "--verbose", "--timeout",
                        str(timeout), "-j" + str(jobs), "--tests-regex",
                        testData[0]
                    ]
                    execute_process("CTest", ctest_command, fh=logFile)
            except Exception as exc:
                with open(logFilePath, encoding="utf8", errors='ignore') as detailedErrorLog:
                    print("An error occured when executing tests '" +
                          testData[0] + "'")
                    print(detailedErrorLog.read())
                raise exc

        ctest_command = [
            CTest.CTEST_COMMAND, "--verbose", "--timeout",
            str(timeout), "-j" + str(jobs)
        ]
        includeRegex = ''
        excludeRegex = ''

        alreadyPerformedTests = '|'.join(
            [testData[0] for testData in self.testPlan])
        if len(alreadyPerformedTests) > 0:
            excludeRegex += alreadyPerformedTests

        if ctcTestRegexIncludeStr is not None:
            includeRegex += ctcTestRegexIncludeStr

        if ctcTestRegexExcludeStr is not None:
            if len(excludeRegex) > 0:
                excludeRegex += '|'
            excludeRegex += ctcTestRegexExcludeStr

        if len(includeRegex) > 0:
            ctest_command.append('--tests-regex')
            ctest_command.append(includeRegex)
        if len(excludeRegex) > 0:
            ctest_command.append('--exclude-regex')
            ctest_command.append(excludeRegex)
        execute_process("CTest", ctest_command)
