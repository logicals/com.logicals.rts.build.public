from buildutil import execute_process

import csv


class CppCheck(object):
    CPPCHECK_COMMAND = "cppcheck"

    def __init__(self, baseDir, checkPlanPath, resultFilePath):
        self.baseDir = baseDir
        self.checkPlanPath = checkPlanPath
        self.resultFilePath = resultFilePath

    def check(self):
        with open(self.resultFilePath, 'w') as resultFile:
            execute_process("CppCheck", self.__getCommand(), fherr=resultFile)

    def __getCommand(self):
        cppcheck_command = [CppCheck.CPPCHECK_COMMAND]
        with open(self.checkPlanPath, 'r') as cpf:
            for line in csv.reader(cpf):
                if line[0] == 'include':
                    cppcheck_command += ['-I',
                                         self.__replaceVariables(line[1])]
                elif line[0] == 'includeFile':
                    cppcheck_command += ['--include=' +
                                         self.__replaceVariables(line[1])]
                elif line[0] == 'define':
                    cppcheck_command += ['-D',
                                         self.__replaceVariables(line[1])]
                elif line[0] == 'undefine':
                    cppcheck_command += ['-U',
                                         self.__replaceVariables(line[1])]
                elif line[0] == 'ignore':
                    cppcheck_command += ['-i',
                                         self.__replaceVariables(line[1])]
                elif line[0] == 'suppress':
                    cppcheck_command += ['--suppress=' +
                                         self.__replaceVariables(line[1])]
                elif line[0] == 'jobs':
                    cppcheck_command += ['-j',
                                         self.__replaceVariables(line[1])]
                else:
                    raise ValueError('Unknown option: ' +
                                     line[0] + '=' + line[1])
        cppcheck_command += [self.baseDir]
        cppcheck_command += ['--enable=all', '--force', '--inline-suppr']
        cppcheck_command += ['--xml', '--xml-version=2', '--verbose']
        return cppcheck_command

    def __replaceVariables(self, argument):
        return argument.replace('${BASEDIR}', self.baseDir)
