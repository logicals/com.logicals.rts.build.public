from buildutil import execute_process


class Strip(object):
    COMMAND = "strip"

    def execute(self, binaryfile, toolchain_prefix=""):
        strip_command = [ toolchain_prefix + Strip.COMMAND, "-s", binaryfile ]
        execute_process("Strip", strip_command)




