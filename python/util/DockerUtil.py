import os
import docker
from pathlib import Path


class DockerConfig:
    """
        A DockerConfig consists of:
            toolchain_name: the name of the toolchain
                (usually used as build target for RTS builds)
            docker_os: the docker image os, either "linux" or "windows"
            dockerfile: the relative path of the dockerfile within
                the repository
    """

    def __init__(self, toolchain_name, docker_os, dockerfile):
        self.toolchain = toolchain_name
        self.docker_os = docker_os
        self.dockerfile = dockerfile

    def __str__(self):
        return "toolchain: %s, docker_os: %s, dockerfile: %s" % (
            self.toolchain,
            self.docker_os,
            self.dockerfile,
        )


class DockerConfigHelper:

    def __init__(self, toolchain_dir, toolchain_prefix="",
                 toolchain_suffix=".cmake"):
        self.toolchain_dir = toolchain_dir
        self.toolchain_prefix = toolchain_prefix
        self.toolchain_suffix = toolchain_suffix

    def get_compatible_configs(self):
        client = docker.from_env()
        system_docker_os = client.info()['OSType'].strip()
        print("Docker is configured to run with %s based containers" %
              (system_docker_os))
        return list(filter(lambda config: config.docker_os == system_docker_os,
                           self.get_docker_configs()))

    def get_docker_configs(self):
        configs = []

        toolchain_files = [
            t
            for t in os.listdir(str(Path(self.toolchain_dir).resolve()))
            if os.path.isfile(str(os.path.join(str(self.toolchain_dir), t)))
            and t.startswith(self.toolchain_prefix)
            and t.endswith(self.toolchain_suffix)
        ]

        for toolchain in toolchain_files:
            configs.extend(self.get_configs_for_toolchain(toolchain))

        return configs

    def get_configs_for_toolchain(self, toolchain):
        toolchain_configs = []
        with open(
            str(Path(self.toolchain_dir, toolchain)), "rt"
        ) as toolchain_file:
            for line in toolchain_file:
                if line.startswith("# docker_config="):
                    toolchain_configs.append(
                        self.get_config_for_line(toolchain, line))
        return toolchain_configs

    def get_config_for_line(self, toolchain, line):
        config = line.replace("# docker_config=", "")
        config_entries = config.split(":")
        docker_os = config_entries[0]
        if not (docker_os == "linux" or docker_os == "windows"):
            raise ValueError(
                "docker_os in toolchain is "
                + docker_os
                + '. Allowed values are "linux" or "windows"!'
            )
        dockerfile = config_entries[1]
        return DockerConfig(self.get_toolchain_name(
                            toolchain).strip(),
                            docker_os.strip(),
                            dockerfile.strip())

    def get_toolchain_name(self, toolchain):
        return (
            toolchain.replace(self.toolchain_prefix, "")
            .replace(self.toolchain_suffix, "")
        )
