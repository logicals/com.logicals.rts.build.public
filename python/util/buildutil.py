from __future__ import print_function

import os
import sys
import shutil
import ntpath
import hashlib
from glob import glob
from zipfile import ZipFile, ZIP_DEFLATED
import tarfile
from subprocess import Popen, PIPE, STDOUT
from distutils.spawn import find_executable


def create_and_clean_dir(path):
    if os.path.exists(path):
        shutil.rmtree(path)
    os.makedirs(path)


def extract_zip(zip_file):
    with ZipFile(zip_file) as package:
        package.extractall()


def extract_tar(tar_file):
    if tar_file.lower().endswith('gz'):
        mode = 'r|gz'
    else:
        mode = 'r'
    with tarfile.open(tar_file, mode) as package:
        package.extractall()


def extract_archive(target_dir, archive_file_glob_expr):
    old_pwd = os.getcwd()
    archive_file = glob(archive_file_glob_expr)[0]
    print(archive_file)
    create_and_clean_dir(target_dir)
    os.chdir(target_dir)
    if archive_file.lower().endswith('.zip'):
        extract_zip(archive_file)
    else:
        extract_tar(archive_file)
    os.chdir(old_pwd)


def create_zip(zip_file, files, flat=False):
    with ZipFile(zip_file, 'w', ZIP_DEFLATED) as archive:
        for single_file in files:
            archive.write(single_file, ntpath.basename(single_file))


def calc_md5_from_file(filename):
    return calc_hash_from_file(filename, hashlib.md5())


def calc_sha1_from_file(filename):
    return calc_hash_from_file(filename, hashlib.sha1())


def calc_hash_from_file(filename, hash_obj):
    with open(filename, 'rb') as handle:
        for chunk in iter(lambda: handle.read(4096), b""):
            hash_obj.update(chunk)
    return hash_obj.hexdigest()


def dump_rts3env_version():
    rts3versionFile = os.environ['RTS3_HOME'] + '\\version.txt'
    print("RTS3 environment version:")

    try:
        with open(rts3versionFile, 'r') as f:
            print(f.read())
    except FileNotFoundError as exc:
        print("RTS3 environment Version file could not be found.", file=sys.stderr)
        raise(exc)


def dump_environment():
    print("Using Python " + sys.version)
    print("Environment variables:")
    for key in os.environ.keys():
        print("%30s => %s" % (key, os.environ[key]))


def execute_process(process_name, commands, fh=sys.stdout, fherr=None):
    print("Executing command (working dir \'" +
          os.getcwd() + "\')" + ": " + " ".join(commands))
    fh.flush()
    if fherr is not None:
        fherr.flush()
    process = Popen(commands, stdout=fh,
                    stderr=STDOUT if fherr is None else fherr)
    returncode = process.wait()
    fh.flush()
    if fherr is not None:
        fherr.flush()
    if returncode != 0:
        raise Exception(process_name + " returned an error")


def is_tool_available(tool_name):
    return find_executable(tool_name) is not None
