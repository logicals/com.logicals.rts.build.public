#!/usr/bin/env python
# -*- coding: utf-8 -*-

import click
from http import HTTPStatus
import http.server
import os
import requests
import socket
import socketserver
import urllib
import urllib3
from pathlib import Path

bPath = None


class MyHandler(http.server.SimpleHTTPRequestHandler):
    def __getAbsPath(self, path):
        return Path(bPath, path).resolve()

    def __sendLocalFile(self, absPath):
        if absPath.is_dir():
            self.send_error(HTTPStatus.NOT_FOUND,
                            "Downloading directories is not supported.")
            return
        f = None
        try:
            f = open(absPath, 'rb')
        except OSError:
            self.send_error(HTTPStatus.NOT_FOUND, "File could not be opened.")
            return
        fs = os.fstat(f.fileno())
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-Type", self.guess_type(absPath))
        self.send_header("Content-Length", str(fs[6]))
        self.send_header("Last-Modified", self.date_time_string(fs.st_mtime))
        self.end_headers()
        try:
            self.copyfile(f, self.wfile)
        finally:
            f.close()

    def __saveLocalFile(self, request, absLocalPath):
        if not absLocalPath.parents[0].exists():
            os.makedirs(absLocalPath.parents[0].resolve())
        with open(absLocalPath, "wb") as f:
            f.write(request.content)

    def __forwardRequest(self, request):
        self.send_response(HTTPStatus.OK)
        self.send_header("Content-Type", request.headers['Content-Type'])
        self.send_header("Content-Length",
                         request.headers['Content-Length'])
        self.send_header("Last-Modified", request.headers['Last-Modified'])
        self.end_headers()
        self.wfile.write(request.content)

    def do_GET(self):
        requestPath = urllib.parse.urlsplit(self.path).path
        absLocalPath = self.__getAbsPath(requestPath[1:])
        try:
            credentials = os.environ['ARTIFACTORY_LOGIN'].split(':')
            request = requests.get(
                'https://artifactory.logicals.com' + requestPath,
                auth=(credentials[0], credentials[1]))

            self.__saveLocalFile(request, absLocalPath)
            self.__forwardRequest(request)
        except (socket.gaierror, requests.exceptions.ConnectionError,
                urllib3.exceptions.NewConnectionError,
                urllib3.exceptions.MaxRetryError,
                BrokenPipeError):
            print("Sending local copy for: " + requestPath)
            self.__sendLocalFile(absLocalPath)


@click.command()
@click.option("--port", default=8000, help='TCP port to bind to.')
@click.option("--basePath", default=str(Path(Path.home(), 'logi.cals').resolve()),
              help='Base path to store/serve files to/from.')
def main(port, basepath):
    global bPath
    bPath = basepath
    print("Using base path: " + basepath)
    try:
        with socketserver.TCPServer(("", 8000), MyHandler) as httpd:
            httpd.serve_forever()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
